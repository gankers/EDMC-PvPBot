"""
A wrapper around EDMCs Configuration.
The pattern seen here has been taken from the EDMC-Massacre plugin
See https://github.com/CMDR-WDX/EDMC-Massacres/blob/master/classes/massacre_settings.py
"""

import tkinter as tk
from typing import Callable, cast, TYPE_CHECKING

from version_check import CURRENT_VERSION

if TYPE_CHECKING:
    from edmc_abstractions import IConfig
else:
    IConfig = None  # I <3 Python Typing
    pass


class ConfigurationUIHandler:
    def __init__(self, parent, cmdr: str) -> None:
        self.__parent = parent
        self.__cmdr = cmdr
        self.__settings_changes: dict[str, tk.Variable] = {}
        self.__frame = None

    def init(self, parent):
        self.__frame = parent
        return self.__render()

    def __render(self) -> tk.Frame:
        import myNotebook as nb

        root = cast(nb.Frame, self.__frame)

        input_offset = 10

        frame = nb.Frame(root)  # type: ignore

        self.__settings_changes.clear()
        self.__settings_changes["check_updates"] = tk.BooleanVar(
            value=self.__parent.check_updates
        )
        self.__settings_changes["send_location"] = tk.BooleanVar(
            value=self.__parent.send_location
        )
        self.__settings_changes["allowed_cmdrs"] = tk.StringVar(
            value=",".join(self.__parent.allowed_cmdrs)
        )
        self.__settings_changes["api_key"] = tk.StringVar(value=self.__parent.api_key)
        self.__settings_changes["debug"] = tk.BooleanVar(value=self.__parent.debug)
        self.__settings_changes["whois"] = tk.BooleanVar(value=self.__parent.whois)
        self.__settings_changes["onboarding"] = tk.BooleanVar(
            value=self.__parent.onboarding
        )
        self.__settings_changes["api_backend"] = tk.StringVar(
            value=self.__parent.api_backend
        )
        self.__settings_changes["prerelease"] = tk.BooleanVar(
            value=self.__parent.updating_check_prerelease
        )
        self.__settings_changes["autoupdate"] = tk.BooleanVar(
            value=self.__parent.autoupdate
        )

        self.__settings_changes["reset_squadron"] = tk.BooleanVar(value=False)
        self.__settings_changes["reset_power"] = tk.BooleanVar(value=False)

        nb.Checkbutton(
            frame,
            text="Look for Updates on Startup",
            variable=self.__settings_changes["check_updates"],
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W)
        nb.Checkbutton(
            frame,
            text="Add Location of Kills to Event",
            variable=self.__settings_changes["send_location"],
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W)
        nb.Label(frame, justify=tk.LEFT, text="Allowed CMDRs:").grid(
            column=0, padx=input_offset, sticky=tk.W
        )
        allowed_cmdrs_edit_text = nb.EntryMenu(
            frame, textvariable=self.__settings_changes["allowed_cmdrs"]
        )
        allowed_cmdrs_edit_text.grid(columnspan=2, padx=input_offset, sticky=tk.EW)
        nb.Label(
            frame,
            justify=tk.LEFT,
            text="Enter which CMDRs you want to upload to the PVP Bot. "
            "This will ignore all other CMDRs. \n"
            "If you leave this blank, all CMDRs will be uploaded."
            "Your CMDRs should be comma-separated.\n"
            "For example: 'WDX, Schitt Staynes' will match CMDR WDX and "
            "CMDR Schitt Staynes\n",
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W, pady=0)
        nb.Label(frame, justify=tk.LEFT, text="API Key:").grid(
            column=0, padx=input_offset, sticky=tk.W
        )

        api_key_edit_text = nb.EntryMenu(
            frame, textvariable=self.__settings_changes["api_key"], show="*"
        )
        api_key_edit_text.grid(columnspan=2, padx=input_offset, sticky=tk.EW)

        nb.Checkbutton(
            frame,
            text="Display Debug Information",
            variable=self.__settings_changes["debug"],
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W)
        nb.Checkbutton(
            frame,
            text="Display K/D when scanning CMDRs",
            variable=self.__settings_changes["whois"],
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W)
        nb.Checkbutton(
            frame,
            text="Run Onboarding on next Startup",
            variable=self.__settings_changes["onboarding"],
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W)
        nb.Label(frame, justify=tk.LEFT, text="Backend Address").grid(
            column=0, padx=input_offset, sticky=tk.W
        )
        nb.EntryMenu(frame, textvariable=self.__settings_changes["api_backend"]).grid(
            columnspan=2, padx=input_offset, sticky=tk.EW
        )

        cmdr_squad = self.__parent.get_squadron_for_cmdr(self.__cmdr)
        if cmdr_squad is not None:
            nb.Checkbutton(
                frame,
                text=f"Reset Squadron for CMDR {self.__cmdr}. (currently {cmdr_squad})"
                "\nWe cannot automatically detect missing squadron because"
                "the handling is flaky in EDMC",
                variable=self.__settings_changes["reset_squadron"],
            ).grid(columnspan=2, padx=input_offset, sticky=tk.W)

        cmdr_power = self.__parent.get_power_for_cmdr(self.__cmdr)
        if cmdr_power is not None:
            nb.Checkbutton(
                frame,
                text=f"Reset Power for CMDR {self.__cmdr}. (currently {cmdr_power})\n"
                "We cannot automatically detect missing Power because the handling "
                "is flaky in EDMC",
                variable=self.__settings_changes["reset_power"],
            ).grid(columnspan=2, padx=input_offset, sticky=tk.W)

        nb.Checkbutton(
            frame,
            text="Check for Prereleases",
            variable=self.__settings_changes["prerelease"],
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W)
        nb.Checkbutton(
            frame,
            text="Automatically update",
            variable=self.__settings_changes["autoupdate"],
        ).grid(columnspan=2, padx=input_offset, sticky=tk.W)

        nb.Label(frame, text="", pady=10).grid()
        nb.Label(
            frame,
            text=f"Made by CMDR WDX | Running Version {CURRENT_VERSION()}",
        ).grid(sticky=tk.W, padx=input_offset)

        # inline import to not break unit tests
        from ttkHyperlinkLabel import HyperlinkLabel

        HyperlinkLabel(
            frame,
            text="View the Code on Gitlab",
            background=nb.Label().cget("background"),
            url="https://gitlab.com/gankers/EDMC-PvPBot",
            underline=True,
        ).grid(columnspan=2, sticky=tk.W, padx=input_offset)
        return cast(tk.Frame, frame)

    def commit(self):
        self.__parent.notify_about_changes(self.__settings_changes, self.__cmdr)
        self.__settings_changes.clear()


class Configuration:
    """
    Abstraction around the config store
    """

    def __init__(self, config: IConfig):
        self.__config = config
        self.__ui_handlers: dict[str, ConfigurationUIHandler] = dict()
        self.config_changed_listeners: list[Callable[[Configuration], None]] = []

    @property
    def send_location(self):
        return self.__config.get_bool("send_location", default=True)

    @send_location.setter
    def send_location(self, val: bool):
        self.__config.set("send_location", val)

    @property
    def check_updates(self):
        return self.__config.get_bool("check_updates", default=True)

    @check_updates.setter
    def check_updates(self, value: bool):
        self.__config.set("check_updates", value)

    @property
    def allowed_cmdrs(self):
        as_str = self.__config.get_str("allowed_cmdrs", default="")
        names = [f.strip() for f in as_str.split(",") if len(f.strip()) > 0]
        return names

    @allowed_cmdrs.setter
    def allowed_cmdrs(self, new_list: list[str]):
        as_str = ",".join(new_list)
        self.__config.set("allowed_cmdrs", as_str)

    @property
    def has_commander_filter_enabled(self):
        return len(self.allowed_cmdrs) > 0

    @property
    def journal_dir(self):
        return self.__config.get_str("journaldir")

    @property
    def api_key(self):
        key = str.strip(self.__config.get_str("api_key", default=""))
        if len(key) == 0:
            return None
        return key

    @api_key.setter
    def api_key(self, val: str):
        stripped = str.strip(val)
        self.__config.set("api_key", stripped)

    @property
    def debug(self):
        return self.__config.get_bool("debug", default=False)

    @debug.setter
    def debug(self, value: bool):
        self.__config.set("debug", value)

    @property
    def whois(self):
        return self.__config.get_bool("whois", default=True)

    @whois.setter
    def whois(self, value: bool):
        self.__config.set("whois", value)

    @property
    def onboarding(self):
        return self.__config.get_bool("onboarding", default=True)

    @onboarding.setter
    def onboarding(self, value: bool):
        self.__config.set("onboarding", value)

    @property
    def api_backend(self):
        return self.__config.get_str("api_backend", default="https://api.gankers.org")

    @api_backend.setter
    def api_backend(self, value: str):
        self.__config.set("api_backend", value)

    @property
    def updating_check_prerelease(self):
        return self.__config.get_bool("check_prerelease", default=False)

    @updating_check_prerelease.setter
    def updating_check_prerelease(self, val: bool):
        self.__config.set("check_prerelease", val)

    @property
    def autoupdate(self):
        return self.__config.get_bool("autoupdate", default=False)

    @autoupdate.setter
    def autoupdate(self, val: bool):
        self.__config.set("autoupdate", val)

    def notify_about_changes(self, data: dict[str, tk.Variable], cmdr: str):
        keys = data.keys()
        if "send_location" in keys:
            self.send_location = data["send_location"].get()
        if "check_updates" in keys:
            self.check_updates = data["check_updates"].get()
        if "allowed_cmdrs" in keys:
            as_str = data["allowed_cmdrs"].get()
            new_list = [f.strip() for f in as_str.split(",") if len(f.strip()) > 0]
            self.allowed_cmdrs = new_list
        if "api_key" in keys:
            self.api_key = data["api_key"].get()
        if "debug" in keys:
            self.debug = data["debug"].get()
        if "onboarding" in keys:
            self.onboarding = data["onboarding"].get()
        if "api_backend" in keys:
            self.api_backend = data["api_backend"].get()
        if "reset_squadron" in keys and data["reset_squadron"].get():
            self.set_squadron_for_cmdr(cmdr, None)
        if "reset_power" in keys and data["reset_power"].get():
            self.set_power_for_cmdr(cmdr, None)
        if "whois" in keys:
            self.whois = data["whois"].get()
        if "autoupdate" in keys:
            self.autoupdate = data["autoupdate"].get()
        if "prerelease" in keys:
            self.updating_check_prerelease = data["prerelease"].get()

    # CMDR-specific Settings
    #
    # These are only really needed because one could start up EDMC
    # while already in game.
    def get_squadron_for_cmdr(self, cmdr: str):
        val = self.__config.get_str(f"{cmdr}.squadron_long_name")

        if val is None or len(val) == 0:
            return None
        return val

    def set_squadron_for_cmdr(self, cmdr: str, value: str | None):
        # For some reason I cant pass None here. So an Empty String
        # is effectively a None
        if value is None:
            value = ""
        return self.__config.set(f"{cmdr}.squadron_long_name", value)

    def get_power_for_cmdr(self, cmdr: str):
        val = self.__config.get_str(f"{cmdr}.power")

        if val is None or len(val) == 0:
            return None
        return val

    def set_power_for_cmdr(self, cmdr: str, value: str | None):
        # For some reason I cant pass None here. So an Empty String
        # is effectively a None
        if value is None:
            value = ""
        return self.__config.set(f"{cmdr}.power", value)

    def is_cmdr_valid(self, cmdr: str | None) -> bool:
        if cmdr is None:
            return False
        if not self.has_commander_filter_enabled:
            return True
        return cmdr.upper() in [f.upper() for f in self.allowed_cmdrs]

    def ui(self, cmdr: str) -> ConfigurationUIHandler:
        if cmdr not in self.__ui_handlers.keys():
            self.__ui_handlers[cmdr] = ConfigurationUIHandler(self, cmdr)
        return self.__ui_handlers[cmdr]
