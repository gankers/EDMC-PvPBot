"""
The Onboarding is ran on first startup.
It is used to set up everything :)
"""

from logging import Logger
import threading
import time
import tkinter
from enum import Enum
from typing import Callable, Optional, Any

import requests

from edmc_abstractions import IAPIClient, ISquadronsClient
from historic_handler import (
    HistoricAggregator,
    HistoricStatus,
)
from plugin_config import Configuration
from plugin_util import ui_build_progress_bar, assertNotNone
from shared_types import ThemeStub
from squadron_lookup import open_file

try:
    from theme import theme
except:  # noqa E722
    theme = ThemeStub()


class Onboarding:
    class _Stage(Enum):
        INTRO = 0
        ALLOWED_CMDRS = 1
        API_KEYS = 2
        SEND_SYSTEM = 3
        HISTORIC_WAITING = 4
        HISTORIC_RESULT = 5
        HISTORIC_SUBMITTING = 6
        SUMMARY = 7

    def __init__(
        self,
        parent: tkinter.Frame,
        config: Configuration,
        logger: Logger,
        squadron_client: ISquadronsClient,
        pvp_client: IAPIClient,
        finish_callback: Callable[[], None],
    ):
        assertNotNone(
            [parent, config, logger, squadron_client, pvp_client, finish_callback]
        )
        self.__root = parent
        self.__config = config
        self.__logger = logger
        self.__pvp_client = pvp_client
        self.__squadron_client = squadron_client
        self.__historic = HistoricAggregator(
            self.__logger, self.__squadron_client, self.__pvp_client, self.__config
        )
        self.__finish_callback = finish_callback
        self.__current_stage: Onboarding._Stage = Onboarding._Stage.INTRO
        self.__stage_handlers: dict[Onboarding._Stage, Callable[[], None]] = {
            Onboarding._Stage.INTRO: self.__render_intro,
            Onboarding._Stage.ALLOWED_CMDRS: self.__render_allowed_cmdrs,
            Onboarding._Stage.API_KEYS: self.__render_api_keys,
            Onboarding._Stage.SEND_SYSTEM: self.__render_send_system,
            Onboarding._Stage.HISTORIC_WAITING: self.__render_historic_waiting,
            Onboarding._Stage.HISTORIC_RESULT: self.__render_historic_result,
            Onboarding._Stage.HISTORIC_SUBMITTING: self.__render_submit_procedure,
            Onboarding._Stage.SUMMARY: self.__render_summary,
        }

        self.__background_aggregate_thread: Optional[threading.Thread] = None

        self.__event_data_last_api_response_status_code: Optional[int] = None
        self.__event_data_last_api_exception: Optional[Any] = None
        self.__event_data_background_aggregate_current_state: Optional[
            HistoricStatus
        ] = None
        self.__event_data_event_data_background_aggregate_current_state_trigger: Optional[  # noqa: E501
            Callable[[], None]
        ] = None
        self.__event_historic_submit_message: list[str] = []

    # only_ui_thread
    def render(self):
        for child in self.__root.winfo_children():
            child.destroy()
        self.__stage_handlers[self.__current_stage]()
        self.__root.grid()

    # only_ui_thread
    def __render_intro(self):
        frame = tkinter.Frame(self.__root)

        tkinter.Label(frame, text="PVP Bot Initial Setup", fg="white").grid()
        tkinter.Label(
            frame,
            text="This seems to be your first time running the plugin.\n"
            "Let's set things up for you :)",
        ).grid()
        btn_frame = tkinter.Frame(frame)

        def on_start():
            self.__current_stage = Onboarding._Stage.ALLOWED_CMDRS
            self.render()

        def on_skip():
            self.__config.onboarding = False
            self.__unmount()

        tkinter.Button(btn_frame, text="Start Setup", command=on_start).grid(
            column=0,
            row=0,
        )
        tkinter.Button(btn_frame, text="Skip Setup", command=on_skip).grid(
            column=1,
            row=0,
        )
        btn_frame.grid()
        frame.pack()

    # only_ui_thread
    def __render_allowed_cmdrs(self):
        frame = tkinter.Frame(self.__root)

        tkinter.Label(
            frame, text="PVP Bot Initial Setup | Allowed CMDRs", fg="white"
        ).grid()
        tkinter.Label(
            frame,
            text="If there are some Alts you don't want to show up on the board,\n"
            "use this List. You can enter Commanders as a Comma-Separated List.\n\n"
            "Example: 'WDX, Schitt Staynes' will ignore any CMDRs that\n "
            "are not WDX or Schitt Staynes.\n",
            anchor="e",
            justify=tkinter.LEFT,
        ).grid()

        def handle_continue():
            self.__config.allowed_cmdrs = [
                f.strip() for f in cmdr_input.get().split(",")
            ]
            self.__current_stage = Onboarding._Stage.API_KEYS

            # User pressed on Start, which means they're very likely
            # to go through the entire thing.
            # This gives us enough time to do a historic aggregate
            def background_aggregate_polling_thread():
                threading.Thread(
                    name="pvp-plugin-background-onboarding-aggregate-background-runner",
                    daemon=True,
                    target=self.__historic.start,
                ).start()

                keep_looping = True
                while keep_looping:
                    state = self.__historic.poll()
                    if state.finished:
                        keep_looping = False
                    else:
                        time.sleep(0.5)
                    self.__event_data_background_aggregate_current_state = state
                    if (
                        self.__event_data_event_data_background_aggregate_current_state_trigger
                        is not None
                    ):
                        self.__event_data_event_data_background_aggregate_current_state_trigger()

            threading.Thread(
                name="pvp-plugin-background-onboarding-aggregate-poller",
                daemon=True,
                target=background_aggregate_polling_thread,
            ).start()

            self.render()

        cmdr_input_from_config = ", ".join(self.__config.allowed_cmdrs)
        cmdr_input = tkinter.StringVar(frame, value=cmdr_input_from_config)
        tkinter.Entry(frame, textvariable=cmdr_input).grid(sticky="we")
        tkinter.Button(frame, text="Continue", command=handle_continue).grid()
        theme.update(frame)

        frame.grid()

    # only_ui_thread
    def __render_api_keys(self):
        frame = tkinter.Frame(self.__root)

        tkinter.Label(frame, text="PVP Bot Initial Setup | API Key", fg="white").grid()
        tkinter.Label(
            frame,
            text="You need an API Key to use the Plugin. You can\n"
            "get one by running /pvpregister in the GGI Discord.\n\n"
            "Press enter in the Input Field to test Key.\n",
            anchor="e",
            justify=tkinter.LEFT,
        ).grid()

        api_status_text = tkinter.StringVar(frame, value="Status: Awaiting Token Check")

        api_input_from_config = self.__config.api_key
        api_input = tkinter.StringVar(frame, value=api_input_from_config)

        def save_token_and_continue(_event=None):
            self.__config.api_key = api_input.get()
            self.__current_stage = Onboarding._Stage.SEND_SYSTEM
            self.render()

        api_entry = tkinter.Entry(frame, textvariable=api_input, show="*")

        def api_status_response(_event=None):
            if self.__event_data_last_api_exception is not None:
                api_status_text.set(
                    "Status: Failed to do API Call. Error:\n"
                    + str(self.__event_data_last_api_exception)
                )
                return

            status = self.__event_data_last_api_response_status_code
            if status is None:
                api_status_text.set("Status: Unexpected Status Code not set")

            if status == 200:
                api_status_text.set("Status: All good! Token is valid")
                save_token_and_continue()
            elif status == 401:
                api_status_text.set("Status: 401 - Token is wrong")
            else:
                api_status_text.set(f"Status: {status} - Unexpected Status")

        api_entry.bind("<<Response>>", api_status_response)

        def check_api_key(_event=None):
            api_status_text.set("Status: Consulting Backend about Validity")
            self.__event_data_last_api_exception = None
            self.__event_data_last_api_response_status_code = None

            # daemon-safe
            def api_check_thread_action():
                """
                This function is used as part of a daemonic thread that calls
                the Backend to check if the Token is valid
                """
                try:
                    api_key = api_input.get()
                    if api_key is None or len(api_key.strip()) == 0:
                        self.__event_data_last_api_exception = "API Key is missing!"
                        api_entry.event_generate("<<Response>>")
                        return
                    result = requests.get(
                        f"{self.__config.api_backend}/users/self",
                        headers={"Authorization": f"Bearer {api_key}"},
                    )
                    self.__event_data_last_api_response_status_code = result.status_code
                    api_entry.event_generate("<<Response>>")
                except Exception as ex:
                    self.__event_data_last_api_exception = ex
                    api_entry.event_generate("<<Response>>")

            # Start a daemon thread. We don't want to block and lock up the UI while
            # we check the API
            threading.Thread(target=api_check_thread_action, daemon=True).start()

        api_entry.bind("<Return>", check_api_key)
        api_entry.grid(sticky="we")

        btn_frame = tkinter.Frame(frame)
        tkinter.Button(btn_frame, text="Test Key", command=check_api_key).grid(
            column=0,
            row=0,
        )
        tkinter.Button(btn_frame, text="Skip", command=save_token_and_continue).grid(
            column=1,
            row=0,
        )
        theme.update(btn_frame)
        btn_frame.grid()

        tkinter.Label(frame, textvariable=api_status_text).grid()

        theme.update(frame)
        frame.pack()

    # only_ui_thread
    def __render_send_system(self):
        frame = tkinter.Frame(self.__root)

        tkinter.Label(
            frame, text="PVP Bot Initial Setup | Send System", fg="white"
        ).grid()
        tkinter.Label(
            frame,
            text="You can chose to opt-out of sending the System where a Kill / Death\n"
            "has happened. Do note that if the other CMDR is running the plugin\n"
            "system data may still be present. Also do note that you can add\n"
            "system data later with a Historic Aggregate.",
            anchor="e",
            justify=tkinter.LEFT,
        ).grid()

        system_variable = tkinter.BooleanVar(frame, value=self.__config.send_location)
        tkinter.Checkbutton(
            frame, text="Send System Data?", variable=system_variable
        ).grid(sticky="we")

        def handle_continue(_event=None):
            self.__config.send_location = system_variable.get()
            self.__current_stage = Onboarding._Stage.HISTORIC_WAITING
            self.render()

        tkinter.Button(frame, text="Continue", command=handle_continue).grid()

        theme.update(frame)
        frame.grid()

    # only_ui_thread
    def __render_historic_waiting(self):
        if (
            self.__event_data_background_aggregate_current_state is not None
            and self.__event_data_background_aggregate_current_state.finished
        ):
            # Already done! Go directly to summary
            self.__current_stage = Onboarding._Stage.HISTORIC_RESULT
            self.render()
            return

        frame = tkinter.Frame(self.__root)

        tkinter.Label(
            frame, text="PVP Bot Initial Setup | Awaiting Aggregate", fg="white"
        ).grid()
        tkinter.Label(
            frame,
            text="Please Hang Tight while an initial\n"
            "Historic Aggregate is done. You\n"
            "will get to choose if you want to upload\n"
            "the result.",
            anchor="e",
            justify=tkinter.LEFT,
        ).grid()

        progress_text = tkinter.StringVar(frame, value="-finding and sorting logs-")

        tkinter.Label(frame, textvariable=progress_text).grid()

        theme.update(frame)

        frame.grid()

        def handle_update():
            event = self.__event_data_background_aggregate_current_state
            if event is None:
                progress_text.set("Uh Oh.. Missing State :(")
            elif event.finished:
                self.__current_stage = Onboarding._Stage.HISTORIC_RESULT
                # Unsubscribe as we no longer need updates
                self.__event_data_event_data_background_aggregate_current_state_trigger = None  # noqa: E501
                self.render()
            else:
                fraction = float(event.logs_handled) / float(event.total_logs)
                progress_text.set(
                    f"[{ui_build_progress_bar(fraction, 10)}] [{event.logs_handled} / {event.total_logs}]"  # noqa: E501
                )

        self.__event_data_event_data_background_aggregate_current_state_trigger = (
            handle_update
        )

    # only_ui_thread
    def __render_historic_result(self):
        frame = tkinter.Frame(self.__root)

        tkinter.Label(
            frame, text="PVP Bot Initial Setup | Aggregate Result", fg="white"
        ).grid()

        if (
            self.__event_data_background_aggregate_current_state is None
            or self.__event_data_background_aggregate_current_state.kill_events is None
        ):
            self.__logger.error(
                "Inconsistent state: called render historic result, "
                "but no state provided"
            )
            return

        event_count = len(
            self.__event_data_background_aggregate_current_state.kill_events
        )
        tkinter.Label(
            frame,
            text=f"Received {event_count} Kill / Death events\n"
            f"Do you want to upload them now?\n\n"
            f"You can also upload them later by going to your settings and\n"
            f"checking 'Run Onboarding on next Startup'.",
        ).grid()

        if (
            self.__event_data_background_aggregate_current_state.squadron_lookup
            is not None
            and self.__event_data_background_aggregate_current_state.squadron_lookup.has_missing_data  # noqa: E501
        ):
            warning_frame = tkinter.Frame(
                frame, highlightbackground="red", highlightthickness=2
            )

            tkinter.Label(
                warning_frame,
                text="WARNING: Some Squadron Names could not be Mapped to a Tag.\n"
                "Please use the Button below to do so manually\n"
                "You can ignore this warning, which will skip\n"
                "Unknown squadrons.",
            ).grid(column=0, row=0, columnspan=2)

            tkinter.Button(
                warning_frame,
                text="Open Squadron Cache",
                command=lambda: open_file(
                    self.__squadron_client.get_cache_path(), self.__logger
                ),
            ).grid(
                column=0,
                row=1,
            )

            def handle_error_reload(_event=None):
                # This should best be offloaded to another Thread,
                # but… would add more complexity than needed.
                # I guess we can live w/ the fact the UI blocks here for a lil bit.

                squads_to_refetch = (
                    []
                    if self.__event_data_background_aggregate_current_state is None
                    or self.__event_data_background_aggregate_current_state.squadron_lookup  # noqa: E501
                    is None
                    else self.__event_data_background_aggregate_current_state.squadron_lookup.requested_names  # noqa: E501
                )

                result = self.__squadron_client.get_data_for_list_of_squadrons(
                    squads_to_refetch
                )

                if self.__event_data_background_aggregate_current_state is not None:
                    self.__event_data_background_aggregate_current_state.squadron_lookup = result  # noqa: E501
                else:
                    self.__logger.warn(
                        "Unexpected squadron looking is empty. Should never happen"
                    )
                self.render()

            tkinter.Button(
                warning_frame, text="Reload", command=handle_error_reload
            ).grid(
                column=1,
                row=1,
            )

            theme.update(warning_frame)
            warning_frame.grid()

        def handle_submit(_event=None):
            self.__current_stage = Onboarding._Stage.HISTORIC_SUBMITTING
            self.render()

        btn_frame = tkinter.Frame(frame)
        tkinter.Button(
            btn_frame, text="Submit Aggregate Events", command=handle_submit
        ).grid(
            column=0,
            row=0,
        )

        def handle_skip():
            self.__current_stage = Onboarding._Stage.SUMMARY
            self.render()

        tkinter.Button(btn_frame, text="Skip", command=handle_skip).grid(
            column=1,
            row=0,
        )
        theme.update(btn_frame)
        btn_frame.grid()

        theme.update(frame)
        frame.grid()
        self.__root.update()

    # only_ui_thread
    def __render_submit_procedure(self):
        frame = tkinter.Frame(self.__root)

        content = tkinter.StringVar(value="")
        tkinter.Label(frame, textvariable=content).grid()

        def update_status(_event=None):
            sse_len = len(self.__event_historic_submit_message)
            if sse_len == 0:
                return
            content.set(self.__event_historic_submit_message[sse_len - 1])

        def handle_finish(_event=None):
            self.__logger.info("Finished Historic Aggregate")
            self.__current_stage = Onboarding._Stage.SUMMARY
            self.render()

        def handle_fail(_event=None):
            content.set("Something failed. Please consult the logs")

            def handle_post_fail():
                time.sleep(2)
                frame.event_generate("<<Finish>>")

            threading.Thread(daemon=True, target=handle_post_fail).start()

        frame.bind("<<Update>>", update_status)  # TODO: This is no longer needed
        frame.bind("<<Failed>>", handle_fail)
        frame.bind("<<Finish>>", handle_finish)

        theme.update(frame)
        frame.pack()

        # UI set up, now actually trigger the thingy
        def finished(message: str):
            self.__event_historic_submit_message = [message]
            frame.event_generate("<<Update>>")

            def handle_post_finish():
                time.sleep(2)
                frame.event_generate("<<Finish>>")

            threading.Thread(daemon=True, target=handle_post_finish).start()

        if self.__event_data_background_aggregate_current_state is not None:

            def upload_task():
                data = self.__event_data_background_aggregate_current_state
                if data is None:
                    raise ValueError("data cannot be null")
                if data.kill_events is None:
                    raise ValueError("kill events cannot be null")
                if data.squadron_lookup is None:
                    raise ValueError("squadron_lookup cannot be null")
                # vvv long running, blocking
                error = self.__pvp_client.submit_historic_kills(
                    data.kill_events, data.squadron_lookup
                )
                if error is not None:
                    self.__logger.error(f"failed to submit Historic Kills: {error}")
                    frame.event_generate("<<Failed>>")
                else:
                    frame.event_generate("<<Finish>>")

            threading.Thread(
                daemon=True, target=upload_task, name="pvp-historic-upload-task"
            ).start()
        else:
            self.__logger.error(
                "Cannot start background aggregate because current state is None."
            )

    # only_ui_thread
    def __render_summary(self):
        frame = tkinter.Frame(self.__root)
        frame.bind("<<TearDown>>", lambda _: self.__unmount(True))
        tkinter.Label(
            frame,
            text="Setup complete! You can find all Plugins Settings under \n"
            "File → Settings → EDMC_PVP_Bot\n"
            "You can also rerun Onboarding from there.\n\n"
            "Switching to main UI in 5s",
        ).grid()
        theme.update(frame)
        frame.pack()

        # daemon-safe
        def delayed_unmount(_event=None):
            time.sleep(5)
            frame.event_generate("<<TearDown>>")

        threading.Thread(
            name="pvpplugin-onboarding-teardown-waiter",
            daemon=True,
            target=delayed_unmount,
        ).start()

    # only_ui_thread
    def __unmount(self, mount_main_ui=True):
        self.__config.onboarding = False
        for child in self.__root.winfo_children():
            child.destroy()
        if mount_main_ui:
            self.__finish_callback()
