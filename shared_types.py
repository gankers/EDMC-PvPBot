"""
Contains types that can be used and imported by both real and test code
This module may only import standard library and provided libraries like requests.
"""

from dataclasses import dataclass
from datetime import datetime
from logging import Logger
from typing import Optional


class ThemeStub:
    def update(self, arg):
        pass


@dataclass
class SquadronData:
    tag: Optional[str]
    name: Optional[str]


@dataclass
class DebugEventHandlerState:
    """
    This is used for debugging purposes.
    If the debug Setting is set, the UI will display the most recent Handler's State
    This Dataclass is used to "transmit" that state over
    """

    system: Optional[str]
    own_cmdr: str
    own_ship: Optional[str]
    target_cache: dict[str, tuple[datetime, str]]
    own_rank: int
    own_squadron: Optional[str]


@dataclass
class UiStateTimers:
    display_success_event_count = 0
    display_error_event_count = 0


@dataclass
class PvpKill:
    system: str | None
    killer: str
    killer_ship: Optional[str]
    killer_rank: int
    killer_squadron: Optional[SquadronData]
    killer_power: Optional[str]
    victim: str
    victim_ship: Optional[str]
    victim_rank: int
    victim_power: Optional[str]
    victim_squadron: Optional[SquadronData]
    kill_timestamp: datetime

    def as_dict(
        self,
        own_squadron_lookup: dict[str, str],
        send_location=True,
        logger: Optional[Logger] = None,
    ):
        victim_squadron_tag: Optional[str] = None
        killer_squadron_tag: Optional[str] = None
        if self.killer_squadron is not None:
            if self.killer_squadron.tag is None:
                if self.killer_squadron.name is None:
                    if logger is not None:
                        logger.error(
                            "Inconsistency: Killer Squadron Name is missing when it "
                            "should have been present. "
                            "Falling back to assuming no squadron"
                        )
                else:
                    self.killer_squadron.tag = own_squadron_lookup.get(
                        self.killer_squadron.name
                    )
                    # The Lookup is only needed for instances where the Short
                    # name is missing -> in other words,
                    # this is only needed for our CMDR.
            killer_squadron_tag = self.killer_squadron.tag
        if self.victim_squadron is not None:
            if self.victim_squadron.tag is None:
                if self.victim_squadron.name is None:
                    if logger is not None:
                        logger.error(
                            "Inconsistency: Victim Squadron Name is missing when it "
                            "should have been present. "
                            "Falling back to assuming no squadron"
                        )
                else:
                    self.victim_squadron.tag = own_squadron_lookup.get(
                        self.victim_squadron.name
                    )
            victim_squadron_tag = self.victim_squadron.tag

        return_dict = {
            "system": self.system,
            "killer": self.killer,
            "victim": self.victim,
            "killer_rank": self.killer_rank,
            "victim_rank": self.victim_rank,
            "kill_timestamp": self.kill_timestamp.isoformat(),
        }

        if send_location:
            return_dict["system"] = self.system

        if self.killer_ship is not None:
            return_dict["killer_ship"] = self.killer_ship
        if self.victim_ship is not None:
            return_dict["victim_ship"] = self.victim_ship
        if killer_squadron_tag is not None:
            return_dict["killer_squadron"] = killer_squadron_tag
        if victim_squadron_tag is not None:
            return_dict["victim_squadron"] = victim_squadron_tag
        if self.victim_power is not None:
            return_dict["victim_power"] = self.victim_power
        if self.killer_power is not None:
            return_dict["killer_power"] = self.killer_power
        return return_dict


@dataclass
class UiState:
    """
    Main UI is written to have a state object (this class)
    and migrations between the states.
    Whenever the UI does something, a complete rerender is made
    """

    debug_data: Optional[DebugEventHandlerState]
    """
    Set this to a state if debugging. Set to None if not needed
    """
    display_update: bool
    """
    This will render a Button to open the Gitlab Page
    """
    display_error: Optional[str]
    """
    This will render a red text
    """
    display_success: Optional[str]
    """
    This will render a neutral text
    """

    @staticmethod
    def default():
        return UiState(
            debug_data=None,
            display_update=False,
            display_success=None,
            display_error=None,
        )


@dataclass
class SquadronLookupResult:
    data: dict[str, str]
    """
    This only contains "resolved" tags. Wont contain placeholders 
    (read: anything with a Length != 4)
    """

    requested_names: list[str]

    has_missing_data: bool
    """
    Used to tell the User that some Squadrons could not be fetched. 
    UI should then have option to open file and tell
    user to enter missing Tags or Skip
    """


@dataclass
class HistoricStatus:
    finished: bool
    logs_handled: int
    total_logs: int
    kill_events: Optional[list[PvpKill]]
    """
    only sent when finished is True
    """
    squadron_lookup: Optional[SquadronLookupResult]
    """
    only sent when finished is True
    """

    @staticmethod
    def default():
        return HistoricStatus(
            finished=False,
            logs_handled=0,
            total_logs=-1,
            kill_events=None,
            squadron_lookup=None,
        )


@dataclass
class PvpWhoisResponse:
    name: str
    kills: int
    deaths: int
    combat_logger: bool
