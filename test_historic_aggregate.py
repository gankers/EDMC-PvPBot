"""
This Test is used to determine that our Event Handler works as we expect.
It contains (anonymized) journal files and our expected result from said files.
"""

import json
import time
import datetime
import pytest

from event_handler import EventHandler, push_event_to_handler
from mock_config import TestMockConfig
from plugin_config import Configuration
from shared_types import PvpKill, SquadronData


def _open_journal_file(filename: str) -> str:
    with open(f"./test_historic_sources/{filename}.log-anon", "r") as file:
        return file.read()


@pytest.mark.parametrize(
    "name, journal_name, expected",
    [
        (
            "was in vulture, moved into mandalay before kills, expected ",
            "Journal.2024-11-06T000421.01",
            [
                PvpKill(
                    system="Deciat",
                    killer="SUBMITTER",
                    killer_ship="mandalay",
                    killer_rank=6,
                    killer_squadron=SquadronData(tag=None, name="SUBMITTER_SQUADRON"),
                    victim="DARKHASEO86",
                    victim_ship="mandalay",
                    victim_rank=0,
                    victim_squadron=SquadronData(tag=None, name=None),
                    kill_timestamp=datetime.datetime(
                        2024, 11, 6, 6, 19, 52, tzinfo=datetime.timezone.utc
                    ),
                    victim_power=None,
                    killer_power="Archon Delaine",
                ),
                PvpKill(
                    system="Akheilos",
                    killer="SUBMITTER",
                    killer_ship="mandalay",
                    killer_rank=6,
                    killer_squadron=SquadronData(tag=None, name="SUBMITTER_SQUADRON"),
                    victim="Ethan Cenick",
                    victim_ship="mandalay",
                    victim_rank=5,
                    victim_squadron=SquadronData(tag="SGSA", name=None),
                    kill_timestamp=datetime.datetime(
                        2024, 11, 6, 7, 2, 32, tzinfo=datetime.timezone.utc
                    ),
                    victim_power=None,
                    killer_power="Archon Delaine",
                ),
            ],
        ),
        (
            "Two kills, both times killer is in a mandalay",
            "Journal.2024-11-10T083613.01",
            [
                PvpKill(
                    system="LTT 4487",
                    killer="SUBMITTER",
                    killer_ship="mandalay",
                    killer_rank=12,
                    killer_squadron=SquadronData(tag=None, name="SUBMITTER_SQUADRON"),
                    victim="Lucky_Tiger",
                    victim_ship="anaconda",
                    victim_rank=2,
                    victim_squadron=SquadronData(tag=None, name=None),
                    kill_timestamp=datetime.datetime(
                        2024, 11, 10, 9, 18, 22, tzinfo=datetime.timezone.utc
                    ),
                    victim_power=None,
                    killer_power="Jerome Archer",
                ),
                PvpKill(
                    system="LTT 4487",
                    killer="SUBMITTER",
                    killer_ship="mandalay",
                    killer_rank=12,
                    killer_squadron=SquadronData(tag=None, name="SUBMITTER_SQUADRON"),
                    victim="Utilizator [EG]",
                    victim_ship="anaconda",
                    victim_rank=3,
                    victim_squadron=SquadronData(tag=None, name=None),
                    kill_timestamp=datetime.datetime(
                        2024, 11, 10, 9, 19, 39, tzinfo=datetime.timezone.utc
                    ),
                    victim_power=None,
                    killer_power="Jerome Archer",
                ),
            ],
        ),
        (
            "Dying to CMDR, both having pledged",
            "Journal-dying-to-pledge",
            [
                PvpKill(
                    system="Asvinnut",
                    killer="YASKA SHEPERD",
                    killer_ship="python_nx",
                    killer_rank=7,
                    killer_squadron=SquadronData(tag="KUMO", name=None),
                    victim="SUBMITTER",
                    victim_ship="dolphin",
                    victim_rank=10,
                    victim_squadron=SquadronData(tag=None, name="SUBMITTER_SQUADRON"),
                    kill_timestamp=datetime.datetime(
                        2024, 11, 20, 21, 31, 50, tzinfo=datetime.timezone.utc
                    ),
                    victim_power="Aisling Duval",
                    killer_power="Archon Delaine",
                ),
            ],
        ),
        (
            "Switching from Anaconda to Mandalay, then killing someone",
            "conda-switched-to-manadalay",
            [
                PvpKill(
                    system="Wyrd",
                    killer="SUBMITTER",
                    killer_ship="mandalay",
                    killer_rank=10,
                    killer_squadron=SquadronData(tag=None, name="SUBMITTER_SQUADRON"),
                    victim="Aten Zeldinry",
                    victim_ship="adder",
                    victim_rank=5,
                    victim_squadron=SquadronData(tag="NORG", name=None),
                    kill_timestamp=datetime.datetime(
                        2024, 11, 24, 21, 37, 26, tzinfo=datetime.timezone.utc
                    ),
                    victim_power=None,
                    killer_power="Archon Delaine",
                ),
            ],
        ),
    ],
)
def test_journal(name, journal_name, expected):
    _ = name
    content = _open_journal_file(journal_name)
    inner_config = TestMockConfig()
    config = Configuration(inner_config)
    handler = EventHandler(config, None, "SUBMITTER", 5)
    kills: list[PvpKill] = []
    handler.set_update_callback(lambda x: kills.append(x))
    for entry in content.splitlines():
        push_event_to_handler(handler, json.loads(entry), None)
    time.sleep(0.2)

    assert expected == kills
