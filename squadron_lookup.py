"""
The Journal does NOT contain the Short Tag of a CMDR's Squadron.
The ShipTargeted Events contain Tags (but not the Long Names) of a Squadron.

To solve this, the Plugin creates a Lookup Json which maps the long name
to the short tag.

We can either do so manually (ask the user pretty please to write the Tag),
or fetch the Data from ED's Backend.

If we do the latter, we need to use the API Key that EDMC keeps hold of.
"""

import json
from logging import Logger
import os.path
import subprocess
import sys
from typing import Optional

import requests

from edmc_abstractions import ISquadronsClient
from plugin_config import Configuration
from shared_types import SquadronLookupResult, PvpKill
from version_check import CURRENT_VERSION


class _Cache:
    def __init__(self, file_path: str):
        self.__file_path = file_path
        self.__state: dict[str, str] = {}
        self.refetch()

    def refetch(self):
        if not os.path.exists(self.__file_path):
            # Nothing to fetch
            self.__state.clear()
        else:
            with open(self.__file_path, "r") as file:
                self.__state = json.load(file)

    def get(self, name: str):
        return self.__state.get(name)

    def __update_json(self):
        with open(self.__file_path, "w") as file:
            json.dump(self.__state, file, indent=2)

    def put_entry(self, long_name: str, short_tag: str):
        self.__state[long_name] = short_tag
        self.__update_json()

    def put_multiple_entries(self, name_and_tag_list: list[tuple[str, str]]):
        for key, value in name_and_tag_list:
            self.__state[key] = value
        self.__update_json()


class SquadronsClient(ISquadronsClient):
    def __init__(self, config: Configuration, logger: Logger) -> None:
        super().__init__()
        self.__version = CURRENT_VERSION()
        self.__config = config
        self.__logger = logger
        self.__json_path = os.path.expanduser(
            os.path.join(config.journal_dir, "PVP_Bot_Squadron_Cache.json")
        )
        self.__cache = _Cache(self.__json_path)

    def get_cache_path(self) -> str:
        return self.__json_path

    def fill_pvp_kill(self, event: PvpKill) -> PvpKill:
        if event.killer_squadron is not None and event.killer_squadron.tag is None:
            event.killer_squadron.tag = _fetch_squadron(
                event.victim_squadron.name, self.__version
            )
        if event.victim_squadron is not None and event.victim_squadron.tag is None:
            event.victim_squadron.tag = _fetch_squadron(
                event.victim_squadron.name, self.__version
            )
        return event

    # daemon only
    def get_data_for_list_of_squadrons(
        self, squadron_names: list[str]
    ) -> SquadronLookupResult:
        """
        Warning! Expected to be called from the non-main thread
        (basically from a daemon)
        """
        self.__cache.refetch()

        data_to_fetch: list[str] = []

        for entry in squadron_names:
            if entry is None:
                continue
            cache_entry = self.__cache.get(entry)
            if cache_entry is None or len(cache_entry) != 4:
                data_to_fetch.append(entry)

        if len(data_to_fetch) > 0:
            self.__logger.info(
                "Missing info on some Squadron Tags. Will try to get them from API."
                f"Missing Squadrons: {', '.join(data_to_fetch)}"
            )

        def _fallback_placeholder(value: str | None):
            if value is None:
                return "REPLACE_ME"
            return value

        data_to_put_in_cache = [
            (f, _fallback_placeholder(_fetch_squadron(f, self.__version)))
            for f in data_to_fetch
        ]

        self.__cache.put_multiple_entries(data_to_put_in_cache)

        return_data: dict[str, str] = {}
        has_missing = False

        for name in squadron_names:
            result = self.__cache.get(name)
            if result is None or len(result) != 4:
                has_missing = True
            else:
                return_data[name] = result

        return SquadronLookupResult(
            data=return_data,
            has_missing_data=has_missing,
            requested_names=squadron_names,
        )


# daemon-only!
def _fetch_squadron(name: str, version: str) -> Optional[str]:
    """
    Takes a Squadron Full Name and potentially returns a Squadron Short Tag.

    If Tag cannot be fetched, either because it does not exist on the Server,
    or because the Server is unavailable, return None
    """

    try:
        result = requests.get(
            f"https://sapi.demb.uk/api/squads/now/by-name/{name}",
            headers={"User-Agent": f"EDMCPvpPlugin-{version}"},
        ).json()

        filtered_data = [f for f in result if "platform" in f and f["platform"] == "PC"]
        if len(filtered_data) == 0:
            return None
        return filtered_data[0]["tag"]
    except Exception:
        return None


def open_file(path: str, logger: Logger):
    """
    Opens link to the Download URL in Browser
    """

    match sys.platform:
        case "darwin":
            subprocess.Popen(["open", path])
        case "win32":
            os.startfile(path)  # pyright: ignore API only present under Windows
        case _:
            try:
                subprocess.Popen(["xdg-open", path])
            except OSError:
                logger.error("Failed to open URL")
