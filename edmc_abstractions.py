"""
EDMC Plugins are a PITA to test without actually running EDMC

Because of this we define an abstraction to EDMC, so we can spy and
mock stuff as we please. Anything provided by EDMC is proxied here
"""

import threading
import time
from abc import ABCMeta, abstractmethod
from datetime import datetime
from logging import Logger
import tkinter
from typing import Any, Callable, Optional, TYPE_CHECKING

from shared_types import HistoricStatus, PvpKill, SquadronLookupResult, PvpWhoisResponse
from version_check import UpdateInfo

if TYPE_CHECKING:
    from plugin_config import Configuration
else:
    Configuration = None  # TODO: add type keyword once EDMC is on Python 3.12
    pass
    # god, I *hate*, absolutely and utterly detest Pythons Typing

Error = Optional[str]  # TODO: add type keyword once EDMC is on Python 3.12
"""
try-catch is a shitty design pattern. Rust and Go do it better by returning an 
explicit error if something can error.
"""


class IConfig(object, metaclass=ABCMeta):
    """
    Proxy around EDMC's Key-Value-Store (or a mock :) )
    """

    @abstractmethod
    def set(self, key: str, val: Any):
        pass

    @abstractmethod
    def get_str(self, key: str, default=None) -> str:
        pass

    @abstractmethod
    def get_bool(self, key: str, default=None) -> bool:
        pass

    @abstractmethod
    def get_app_name(self) -> str:
        pass


class IAPIClient(object, metaclass=ABCMeta):
    """
    Client responsible for talking to api.gankers.org (or our mock)
    """

    @abstractmethod
    def check_api_key(self, key: str) -> Error:
        pass

    @abstractmethod
    def submit_historic_kills(
        self, payload: list[PvpKill], squadron_lookup: SquadronLookupResult
    ) -> Error:
        pass

    @abstractmethod
    def submit_live_kill(
        self, payload: PvpKill, squadron_lookup: SquadronLookupResult
    ) -> Error:
        pass

    @abstractmethod
    def get_by_name(self, target) -> tuple[Optional[PvpWhoisResponse], Error]:
        pass


class ISquadronsClient(object, metaclass=ABCMeta):
    """
    REST Client tasked with figuring out Squadron Tags based on Squadron Name
    """

    @abstractmethod
    def get_data_for_list_of_squadrons(
        self, squadron_names: list[str]
    ) -> SquadronLookupResult:
        """
        Returns a Go-like Value, Error Tuple.
        Only consume 1st value if 2nd value (error) is None
        """
        pass

    @abstractmethod
    def get_cache_path(self) -> str:
        pass

    @abstractmethod
    def fill_pvp_kill(self, event: PvpKill) -> PvpKill:
        pass


class IUiHandler(object, metaclass=ABCMeta):
    @abstractmethod
    def notify_about_outdated(self):
        pass

    @abstractmethod
    # daemon-safe
    @abstractmethod
    def write_happy_message(self, message: str, duration_secs: float):
        pass

    # daemon-safe
    @abstractmethod
    def write_error_message(self, message: str, duration_secs: float):
        pass

    @abstractmethod
    def write_debug_info(self, handler):
        pass


class IEventHandler(object, metaclass=ABCMeta):
    @abstractmethod
    def put_own_rank(self, rank: str):
        pass

    @abstractmethod
    def put_system_update(self, system: str):
        pass

    @abstractmethod
    def clear_cache(self):
        pass

    @abstractmethod
    def put_ship_update(self, ship_name: str):
        pass

    @abstractmethod
    def put_targeted_event(
        self,
        targeted_cmdr: str,
        targeted_ship: str,
        ts: datetime,
        targeted_squadron: Optional[str],
        targeted_power: Optional[str],
    ):
        pass

    @abstractmethod
    def put_own_squadron(self, squadron_long_name: Optional[str]):
        pass

    @abstractmethod
    def put_own_power(self, power_name: Optional[str]):
        pass

    @abstractmethod
    def put_kill(self, victim: str, victim_rank: int, ts: datetime):
        pass

    @abstractmethod
    def put_death(
        self, killer: str, killer_rank: int, killer_ship: Optional[str], ts: datetime
    ):
        pass

    @abstractmethod
    def enable_ui_push(self):
        pass

    @abstractmethod
    def set_update_callback(self, cb: Callable[[PvpKill], None]):
        """
        Event Handler impl is expected to trigger the callback from
        its own daemon thread.
        Callback return None if all is ok and an Error String if something is no bueno
        """
        pass

    @abstractmethod
    def set_targeted_callback(self, cb: Optional[Callable[[str], None]]):
        """
        Event Handler impl is expected to trigger the callback for any thread.
        Main thread is fine.
        """
        pass


class EDMCProxy(object, metaclass=ABCMeta):
    """
    The "root" object. Contains all the interactions to EDMC / the "real" world
    """

    @abstractmethod
    def logger(self) -> Logger:
        pass

    @abstractmethod
    def raw_config(self) -> IConfig:
        pass

    @abstractmethod
    def pvp_api(self) -> IAPIClient:
        pass

    @abstractmethod
    def squadrons_api(self) -> ISquadronsClient:
        pass

    @abstractmethod
    def config(self) -> Configuration:
        pass

    @abstractmethod
    def ui(self) -> IUiHandler:
        pass

    @abstractmethod
    def plugin_name(self) -> str:
        pass

    @abstractmethod
    def put_parent_frame(self, parent: tkinter.Frame) -> None:
        pass

    live_handlers: dict[str, IEventHandler] = {}

    @abstractmethod
    def update_handler(self) -> UpdateInfo:
        pass

    def do_api_key_check(self):
        def thread():
            err = self.pvp_api().check_api_key(self.config().api_key)
            # Wait for UI to be ready
            ttl = 10
            while self.ui() is None and ttl > 0:
                time.sleep(1)
                self.logger().debug("UI not ready yet. Waiting 1s")
                ttl -= 1
            if err is None:
                self.ui().write_happy_message("API Key is valid", 6)
            else:
                self.logger().error(f"PVP Bot: Failed API Key Check: {err}")
                self.ui().write_error_message(f"Failed API Key check: {err}", -1)

        threading.Thread(name="pvp-api-check", daemon=True, target=thread).start()

    def do_whois_lookup(self, cmdr: str):
        if not self.config().whois:
            return

        def thread(target: str):
            data, err = self.pvp_api().get_by_name(target)
            if err is not None:
                self.ui().write_error_message(f"Err trying to whois CMDR: {err}", 10)
                self.logger().error(
                    f"PVP Bot: Failed Whois Request for CMDR {target}: {err}"
                )
                return
            if data is None:
                self.ui().write_happy_message(f"CMDR {target} not known to Bot", 10)
                return
            message = f"CMDR {data.name} @ {data.kills} K / {data.deaths} D"
            if data.combat_logger:
                self.ui().write_error_message(message + " <COMBAT LOGGER>", 10)
            else:
                self.ui().write_happy_message(message, 5)

        threading.Thread(
            name="pvp-whois-check", daemon=True, target=thread, args=[cmdr]
        ).start()


class IHistoricAggregator(object, metaclass=ABCMeta):
    @abstractmethod
    def poll(self) -> HistoricStatus:
        """
        Returns the current status. Main-Thread safe
        """
        pass

    # daemon only
    @abstractmethod
    def start(self):
        """
        Starts collecting historic data.
        WARNING: This is a long-running task. MUST Run from daemon thread
        """
        pass

    # daemon only
    @abstractmethod
    def submit(self):
        """
        Yeets the result against the Backend
        """
        pass
