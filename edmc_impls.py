"""
This module implements the abstractions from edmc_abstractions and wires them up
to EDMC / the "real" system. Stuff in here is untestable. And this file cannot be
imported by tests because imports would eventually lead to EDMC's weird context.

THE ONLY PLACE THIS SHOULD BE IMPORTED IS IN load.py.
"""

import json
import threading
from pathlib import Path
import logging
from os.path import basename, dirname
import tkinter
from typing import Any, Optional

import requests
from edmc_abstractions import (
    EDMCProxy,
    IAPIClient,
    IConfig,
    ISquadronsClient,
    Error,
    IUiHandler,
)
from plugin_config import Configuration
from shared_types import PvpKill, SquadronLookupResult, PvpWhoisResponse
from squadron_lookup import SquadronsClient
from ui_handler import UiHandler
import version_check
from version_check import UpdateInfo


def make_real_edmc_impl() -> EDMCProxy:
    return RealEDMCProxy()


def _build_logger_for_module(app_name: str, plugin_name: str) -> logging.Logger:
    """
    Create the logger for this Plugin in accordance with the EDMC Docs
    """
    logger_name = f"{app_name}.{plugin_name}"
    logger = logging.getLogger(logger_name)

    # Taken from Docs: https://github.com/EDCD/EDMarketConnector/blob/main/PLUGINS.md#logging
    # If the Logger has handlers then it was already set up by the core code, else
    # it needs setting up here.
    if not logger.hasHandlers():
        level = logging.INFO  # So logger.info(...) is equivalent to print()

        logger.setLevel(level)
        logger_channel = logging.StreamHandler()
        logger_formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(module)s:%(lineno)d:%(funcName)s: %(message)s"  # noqa: E501
        )
        logger_formatter.default_time_format = "%Y-%m-%d %H:%M:%S"
        logger_formatter.default_msec_format = "%s.%03d"
        logger_channel.setFormatter(logger_formatter)
        logger.addHandler(logger_channel)
    return logger


class RealEDMCProxy(EDMCProxy):
    def __init__(self) -> None:
        super().__init__()
        self.__plugin_name = basename(Path(dirname(__file__)))
        self.__raw_config = RealConfigProxy(self.__plugin_name)
        self.__config = Configuration(self.__raw_config)
        self.__logger = _build_logger_for_module(
            self.__raw_config.get_app_name(), self.__plugin_name
        )
        self.__pvp_client = RealPvpAPIClient(self.__config, self.__logger)
        self.__squadron_api = SquadronsClient(self.__config, self.__logger)
        self.__ui: Optional[IUiHandler] = None
        self.__update_handler = UpdateInfo(
            self.__logger, self.__plugin_name, self.__config.updating_check_prerelease
        )
        threading.Thread(
            name="edpvp-update-check", daemon=True, target=self.__update_handler.check
        ).start()

    def logger(self) -> logging.Logger:
        return self.__logger

    def raw_config(self) -> IConfig:
        return self.__raw_config

    def config(self) -> Configuration:
        return self.__config

    def pvp_api(self) -> IAPIClient:
        return self.__pvp_client

    def squadrons_api(self) -> ISquadronsClient:
        return self.__squadron_api

    def plugin_name(self) -> str:
        return self.__plugin_name

    def ui(self) -> Optional[IUiHandler]:
        return self.__ui

    def put_parent_frame(self, parent: tkinter.Frame) -> None:
        handler = UiHandler(
            parent,
            self.__config,
            self.__logger,
            self.squadrons_api(),
            self.pvp_api(),
            self.update_handler(),
        )
        self.__ui = handler

    def update_handler(self) -> UpdateInfo:
        return self.__update_handler


class RealConfigProxy(IConfig):
    def __init__(self, plugin_name: str) -> None:
        super().__init__()
        from config import config, appname

        self.__config = config
        self.__plugin_name = plugin_name
        self.__app_name = appname

    def set(self, key: str, val: Any):
        self.__config.set(f"{self.__plugin_name}.{key}", val)

    def get_str(self, key: str, default=None) -> str:
        if key == "journaldir":
            response = self.__config.get_str(key, default=default)
            if response is None or response == "":
                response = self.__config.default_journal_dir
            return response
        return self.__config.get_str(f"{self.__plugin_name}.{key}", default=default)

    def get_bool(self, key: str, default=None) -> bool:
        return self.__config.get_bool(f"{self.__plugin_name}.{key}", default=default)

    def get_app_name(self) -> str:
        return self.__app_name


class RealPvpAPIClient(IAPIClient):
    def __init__(self, config: Configuration, logger: logging.Logger) -> None:
        self.__config = config
        self.__logger = logger
        super().__init__()

    # daemon-only
    def get_by_name(self, target) -> tuple[Optional[PvpWhoisResponse], Error]:
        try:
            result = requests.get(f"{self.__config.api_backend}/elite/cmdrs/{target}")

            match result.status_code:
                case 200:
                    result_json = result.json()
                    if not isinstance(result_json, dict):
                        raise ValueError("Response not a dict. Should never happen")

                    combat_logger = result_json.get("is_combat_logger")
                    if combat_logger is None:
                        combat_logger = False
                    if not isinstance(combat_logger, bool):
                        raise ValueError(
                            "Clogging Field not a bool. Should never happen"
                        )

                    return PvpWhoisResponse(
                        name=result_json["cmdr_name"],
                        kills=result_json["kills"],
                        deaths=result_json["deaths"],
                        combat_logger=combat_logger,
                    ), None
                case 404:
                    return None, None
                case _:
                    self.__logger.error(
                        f"PVP Backend Whois responded with Status {result.status_code}"
                        f"and content {result.text}"
                    )
                    return (
                        None,
                        "PVP Backend Whois responded with unexpected Status"
                        + str(result.status_code),
                    )
        except Exception as ex:
            self.__logger.exception(ex)
            return None, "PVP: Unexpected Error. See EDMC Logs for more info"

    # daemon-only
    def check_api_key(self, key: str) -> Error:
        """
        WARNING: BLOCKING. CALL FROM WORKER THREAD
        """
        if self.__config.api_key is None or len(self.__config.api_key.strip()) == 0:
            return "PVP: API Key is missing"
        try:
            result = requests.get(
                f"{self.__config.api_backend}/users/self",
                headers={
                    "Authorization": f"Bearer {self.__config.api_key.strip()}",
                    "X-Plugin-Version": version_check.CURRENT_VERSION(),
                },
            )

            match result.status_code:
                case 200:
                    return None
                case 401:
                    return "PVP: API Key not valid"
                case 500:
                    return "PVP: Server broke"
                case _:
                    self.__logger.error(
                        "PVP Backend Auth Check responded with Status "
                        f"{result.status_code} and content {result.text}"
                    )
                    return "PVP: Backend responded with unexpected Status " + str(
                        result.status_code
                    )
        except Exception as ex:
            self.__logger.exception(ex)
            return "PVP: Unexpected Error. See EDMC Logs for more info"

    # daemon-only
    def submit_historic_kills(
        self, payload: list[PvpKill], squadron_lookup: SquadronLookupResult
    ) -> Error:
        """
        blocking. Call from daemon
        """
        endpoint = self.__config.api_backend
        token = self.__config.api_key
        payload_raw = [
            f.as_dict(
                squadron_lookup.data,
                send_location=self.__config.send_location,
                logger=self.__logger,
            )
            for f in payload
        ]
        self.__logger.debug(
            "Historic payload was submitted: " + json.dumps(payload_raw)
        )
        response = requests.post(
            f"{endpoint}/elite/submit/historic",
            headers={
                "Authorization": f"Bearer {token}",
                "X-Plugin-Version": version_check.CURRENT_VERSION(),
            },
            json=payload_raw,
        )
        if response.status_code != 200:
            self.__logger.error(
                "[HISTORIC/PVP]: Failed to push data: " + str(response.content)
            )
            return "Unexpected Error trying to call PVP Historic Endpoint"
        else:
            self.__logger.info(
                "[HISTORIC/PVP]: Accepted with body: " + str(response.content)
            )
            return None

    # daemon-only
    def submit_live_kill(
        self, payload: PvpKill, squadron_lookup: SquadronLookupResult
    ) -> Error:
        endpoint = self.__config.api_backend
        token = self.__config.api_key
        json_payload = payload.as_dict(
            squadron_lookup.data, send_location=self.__config.send_location
        )
        try:
            response = requests.post(
                f"{endpoint}/elite/submit",
                headers={
                    "Authorization": f"Bearer {token}",
                    "X-Plugin-Version": version_check.CURRENT_VERSION(),
                },
                json=json_payload,
            )

            if response.status_code == 200:
                return None
            elif response.status_code == 401:
                return "PVP: Unauthenticated. Probably expired token"
            else:
                message: str
                try:
                    message = response.json()["message"]
                except:  # noqa E722
                    message = response.text
                self.__logger.error(
                    f"Live submit returned with status {response.status_code}, which is"
                    f"unexpected. It also contains the message: {message}"
                )
                return f"PVP: Unexpected Status HTTP {response.status_code}"

        except Exception as ex:
            self.__logger.exception(ex)
            return "unexpected error trying to submit live kill."
            +"See EDMC Logs for more info"

        pass
