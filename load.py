"""
This is just the "entry" to the plugin.
Basically nothing in terms of logic is here. It's all in the load_impl.py,
far away from any direct EDMC code, where it can be tested without all the
EDMC Shenanigans.
"""

import tkinter
from typing import Any


from edmc_abstractions import EDMCProxy
from edmc_impls import RealEDMCProxy
from load_impl import (
    journal_entry_impl,
    plugin_app_impl,
    plugin_prefs_impl,
    plugin_start3_impl,
    prefs_changed_impl,
)


def is_called_from_edmc() -> bool:
    try:
        return True
    except:  # noqa: E722
        return False


if not is_called_from_edmc():
    raise NotImplementedError()

plugin: EDMCProxy | None = None


def plugin_app(parent: tkinter.Frame) -> tkinter.Frame:
    if plugin is None:
        raise ValueError("bad state: plugin is none")
    return plugin_app_impl(plugin, parent)


def plugin_start3(_path: str) -> str:
    global plugin
    plugin = RealEDMCProxy()
    if plugin is None:
        raise ValueError("bad state: plugin is none")
    return plugin_start3_impl(plugin)


def plugin_prefs(parent: tkinter.Frame, cmdr: str, _is_beta: bool):
    if plugin is None:
        raise ValueError("bad state: plugin is none")
    return plugin_prefs_impl(plugin, cmdr, parent)


def prefs_changed(cmdr: str, _is_beta: bool):
    if plugin is None:
        raise ValueError("bad state: plugin is none")
    prefs_changed_impl(plugin, cmdr)


def journal_entry(
    cmdr: str,
    _is_beta: bool,
    system: str,
    _station: str,
    entry: dict[str, Any],
    state: dict[str, Any],
):
    if plugin is None:
        raise ValueError("bad state: plugin is none")
    journal_entry_impl(plugin, cmdr, system, entry, state)
