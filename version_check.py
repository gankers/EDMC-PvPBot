"""
This Module contains logic to check for new Updates in a separate Thread
Most of the code here was taken from the EDMC-Massacre plugin.
You can find the original here: https://github.com/CMDR-WDX/EDMC-Massacres/blob/master/classes/version_check.py
"""

import dataclasses
import shutil
import zipfile
from logging import Logger
import os
import subprocess
import sys
import tempfile

import requests
from requests import get
from typing import Optional
from semantic_version import Version

RELEASES_URL = "https://gitlab.com/api/v4/projects/gankers%2FEDMC-PvPBot/releases"


def CURRENT_VERSION():
    """
    We go away from using the version file. at/DEV/at is replaced by the Pipeline to
    show the version from the Tag.
    """
    return "0.0.0-DEV"  # PIPELINE_REPLACE


class UpdateInfo:
    @dataclasses.dataclass
    class Data:
        tag_name: str
        browser_link: str
        zip_link: str

    def __init__(self, logger: Logger, plugin_name: str, allow_prerelease=False):
        self._logger = logger
        self.plugin_name = plugin_name
        self._beta = allow_prerelease
        self._data: Optional[UpdateInfo.Data] = None

    @property
    def remote_version(self):
        return self._data.tag_name

    # thread-only!
    def check(self):
        response = get(RELEASES_URL).json()
        for entry in response:
            if "tag_name" not in entry:
                continue
            tag = entry["tag_name"]
            asset_url: Optional[str] = None
            if "pre" in tag:
                # We have a pre-release
                if not self._beta:
                    continue
            # Here we have a valid version
            links = entry["assets"]["links"]
            for link in links:
                if not link["name"] == f"EDMC_PvP_Bot-v{tag}.zip":
                    continue
                asset_url = link["direct_asset_url"]
            self._data = UpdateInfo.Data(tag, entry["_links"]["self"], asset_url)
            return self._data
        return None

    # thread-only!
    def run_autoupdate(self):
        data = self._data
        if data is None:
            raise ValueError("Missing Release Info")
        self._logger.info("Downloading update from " + data.zip_link)
        response = requests.get(data.zip_link)
        if response.status_code != 200:
            raise ValueError(
                "Unexpected Status Code from HTTP Request. expected Status 200, "
                f"got {response.status_code}"
            )
        with tempfile.TemporaryDirectory() as tmp_dir:
            zip_path = os.path.join(tmp_dir, "tmp.zip")
            with open(zip_path, "wb") as zip_file:
                zip_file.write(response.content)
            with zipfile.ZipFile(zip_path, "r") as zip_ref:
                zip_ref.extractall(tmp_dir)
            self._logger.info("Extracted to temp dir " + zip_path)
            os.remove(zip_path)
            live_file_dir = os.path.dirname(__file__)
            other_dir = os.path.normpath(
                os.path.join(live_file_dir, "..", "_EDPVP_TMP")
            )
            if os.path.exists(other_dir):
                shutil.rmtree(other_dir)
            try:
                self._logger.info("Moving current live files to " + other_dir)
                shutil.move(live_file_dir, other_dir)
                self._logger.info("Moving new files to live " + live_file_dir)

                shutil.move(
                    os.path.normpath(os.path.join(tmp_dir, self.plugin_name)),
                    live_file_dir,
                )
            except Exception as ex:
                self._logger.error("Something failed trying to update plugin")
                self._logger.exception(ex)
                if os.path.exists(live_file_dir):
                    shutil.rmtree(live_file_dir)
                shutil.move(other_dir, live_file_dir)
                raise ex
        self._logger.info("Update is done!")

    def is_current_version_outdated(self):
        current_version = Version.coerce(CURRENT_VERSION())
        server_version = Version.coerce(self._data.tag_name)
        return server_version > current_version

    def open_download_page(self):
        """
        Opens link to the Download URL in Browser
        """

        match sys.platform:
            case "darwin":
                subprocess.Popen(["open", self._data.browser_link])
            case "win32":
                os.startfile(self._data.browser_link)  # pyright: ignore - API only present on Windows
            case _:
                try:
                    subprocess.Popen(["xdg-open", self._data.browser_link])
                except OSError:
                    self._logger.error("Failed to open URL")
