"""
This is a build script. It takes an input *.log file and outputs a *.log-anon file.
*.log files are not commited.
*.log-anon files are stripped of irrelevant events and of identifying information
"""

import json
import os

# Keep this up-to-date with the list in EventHandler
relevant_events = [
    "LoadGame",  # Used for current ship, current CMDR Name
    "Loadout",  # Used when ship is updated
    "Rank",  # Used for own Rank
    "Location",  # StarSystem Data used for current System
    "Embark",  # Used to figure out when we go into an SRV or the Ship
    "SuitLoadout",  # Emitted when disembarking - we use this to figure out our own suit
    "LaunchSRV",  # Emitted when SRV is deployed.
    # We don't care about the DockSRV Event as a Loadout Event always follows
    "SupercruiseEntry",  # Reconcile System
    "FSDJump",  # Update Location
    "ShipTargeted",  # Used to potentially infer a Kill Victim's Ship
    "PVPKill",  # Used when we kill another player. Contains Name, Time and Combat Rank
    "Died",  # Used when killed by another player,
    "SquadronStartup",  # Used to infer the player's Squadron
    "JoinedSquadron",  # Used to infer the player's Squadron
    "DisbandedSquadron",  # Used to infer the player's Squadron
    "KickedFromSquadron",  # Used to infer the player's Squadron
    "LeftSquadron",  # Used to infer the player's Squadron
    "Powerplay",  # Used to infer the player's Power
    # additionally, so we know from when this file is:
    "Fileheader",
]


def transform_file(content: str):
    # Filter out potentially identifying lines
    cmdr_name = ""
    fid = ""
    squadron = None
    relevant_lines: list[str] = []
    for line in content.splitlines():
        newLine = line
        try:
            as_json = json.loads(line)
            event_type = as_json["event"]
            if event_type == "SquadronStartup":
                squadron = as_json["SquadronName"]
            if event_type == "Commander":
                fid = as_json["FID"]
                cmdr_name = as_json["Name"]
            if event_type not in relevant_events:
                continue

            relevant_lines.append(newLine)
        except:  # noqa E722
            continue

    return (
        "\n".join(relevant_lines)
        .replace(cmdr_name, "SUBMITTER")
        .replace(fid, "REDACTED")
        .replace(squadron if squadron is not None else "@@@@@@@@", "SUBMITTER_SQUADRON")
    )


script_directory = os.path.dirname(os.path.abspath(__file__))

# Iterate over all files in the directory
for file_name in os.listdir(script_directory):
    # Check if the file ends with '.log'
    if file_name.endswith(".log"):
        # Full path of the original file
        original_file_path = os.path.join(script_directory, file_name)

        # Construct the new file name
        new_file_name = file_name.replace(".log", ".log-anon")
        new_file_path = os.path.join(script_directory, new_file_name)

        try:
            # Read the original file's content
            with open(original_file_path, "r") as original_file:
                content = original_file.read()

            # Write the modified content to the new file
            with open(new_file_path, "w") as new_file:
                new_file.write(transform_file(content))

            print(f"Transformed: {file_name} -> {new_file_name}")  # noqa: T201

        except Exception as e:
            print(f"Error processing {file_name}: {e}")  # noqa: T201
