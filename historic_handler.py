import datetime
import json
from logging import Logger
import pathlib
from typing import Any, Callable, Optional


from edmc_abstractions import IAPIClient, IHistoricAggregator, ISquadronsClient, Error
from plugin_config import Configuration
from shared_types import HistoricStatus
from event_handler import EventHandler, push_event_to_handler, PvpKill, relevant_events
from log_file_name_utils import get_timestamp_from_filename


def _get_all_relevant_logs(config: Configuration, logger: Logger):
    logs_directory = config.journal_dir
    log_and_timestamp: list[tuple[pathlib.Path, datetime.datetime]] = []

    for file in pathlib.Path(logs_directory).glob("*.log"):
        if not file.is_file():
            continue

        journal_content: str = ""
        try:
            journal_content = file.read_text(errors="ignore")
        except Exception as ex:
            logger.error(f"Failed to read journal {file.name}. Skipping...")
            logger.exception(ex)
            continue

        if not config.is_cmdr_valid(__get_cmdr_from_journal(journal_content, logger)):
            continue
        timestamp = get_timestamp_from_filename(file.name)
        if timestamp is None:
            continue
        log_and_timestamp.append((file, timestamp))
    log_and_timestamp.sort(key=lambda x: x[1])

    # Logs are returned in sorted order. This is important to have a Squadron Tag
    # after historic aggregation
    return [f[0] for f in log_and_timestamp]


def _extract_kill_events_and_own_squadrons(
    relevant_logs: list[pathlib.Path],
    update_callback: Optional[Callable[[HistoricStatus], None]],
    logger: Logger,
    config: Configuration,
) -> tuple[list[PvpKill], list[str]]:
    kill_events: list[PvpKill] = []
    own_squadron_list: list[str] = []

    log_count = len(relevant_logs)
    logger.info(f"Total of {log_count} Logs are to be considered")

    last_update = datetime.datetime.now(datetime.UTC)

    for idx, log in enumerate(relevant_logs):
        if idx % 10 == 0:
            logger.info(f"Handling [{idx + 1}/{len(relevant_logs)}]: {log}")
            current_epoch = datetime.datetime.now(datetime.UTC)
            if update_callback is not None and (
                current_epoch - last_update
            ) > datetime.timedelta(seconds=1):
                last_update = current_epoch
                update_callback(
                    HistoricStatus(
                        finished=False,
                        logs_handled=idx + 1,
                        total_logs=log_count,
                        squadron_lookup=None,
                        kill_events=None,
                    )
                )

        log_text = log.read_text(errors="ignore")

        if '"Died"' not in log_text and '"PVPKill"' not in log_text:
            # We don't care, no PVPKill or Died Events here
            continue

        cmdr_name = __get_cmdr_from_journal(log_text, logger)
        rank_name = __get_rank_from_journal(log_text, logger)
        if cmdr_name is None or rank_name is None:
            logger.error(
                f"Could not handle File {log.name} because failed to infer CMDR or Rank"
            )
            continue
        # We unset the Squadron Name before every file
        # Files are sorted by time.
        # And because we read the ENTIRE file, we always get the Squadron Event
        config.set_squadron_for_cmdr(cmdr_name, None)
        config.set_power_for_cmdr(cmdr_name, None)
        handler = EventHandler(config, logger, cmdr_name, rank=rank_name)

        def callback(kill: PvpKill):
            kill_events.append(kill)

        handler.set_update_callback(callback)

        for line in log_text.splitlines():
            if len(line.strip()) == 0:
                continue

            has_relevant_event = False
            for event_name in relevant_events:
                if event_name in line:
                    has_relevant_event = True
                    break

            if not has_relevant_event:
                continue

            as_json: dict[str, Any]
            try:
                as_json = json.loads(line)
            except:  # noqa E722
                logger.warning(
                    f"Failed to parse line <<{line}>> as JSON in File {log}."
                    "Skipping line."
                )
                continue

            if as_json["event"] == "SquadronStartup":
                own_squadron_list.append(as_json["SquadronName"])
            try:
                push_event_to_handler(handler, as_json, logger)
            except Exception as ex:
                logger.error(
                    "Failed to push data to event handler during Historic "
                    "Aggregate. Journal File: " + log.name
                )
                logger.exception(ex)
    return kill_events, list(set(own_squadron_list))


class HistoricAggregator(IHistoricAggregator):
    def __init__(
        self,
        logger: Logger,
        squadrons_client: ISquadronsClient,
        pvp_client: IAPIClient,
        config: Configuration,
    ) -> None:
        super().__init__()
        self.__logger = logger
        self.__squadrons_client = squadrons_client
        self.__pvp_client = pvp_client
        self.__config = config
        self.__current_status = HistoricStatus.default()

    # daemon only
    def start(self):
        try:
            relevant_logs = _get_all_relevant_logs(self.__config, self.__logger)

            def callback(data: HistoricStatus):
                self.__current_status = data

            historic_events, all_own_squadrons = _extract_kill_events_and_own_squadrons(
                relevant_logs, callback, self.__logger, self.__config
            )

            mapping = self.__squadrons_client.get_data_for_list_of_squadrons(
                all_own_squadrons
            )

            callback(
                HistoricStatus(
                    finished=True,
                    logs_handled=len(relevant_logs),
                    total_logs=len(relevant_logs),
                    kill_events=historic_events,
                    squadron_lookup=mapping,
                )
            )
        except Exception as ex:
            self.__logger.error("Failed to do historic aggregate:" + str(ex))
            self.__logger.exception(ex)

    # daemon-only, long-running
    def submit(self) -> Error:
        status = self.__current_status
        if not status.finished:
            return "Cannot submit - Status not done"
        payload = status.kill_events
        if payload is None:
            return "Cannot submit - Payload is None"
        squadrons = status.squadron_lookup
        if squadrons is None:
            return "Cannot submit - Squadron Info is None"

        return self.__pvp_client.submit_historic_kills(payload, squadrons)

    def poll(self) -> HistoricStatus:
        return self.__current_status


def __get_cmdr_from_journal(contents: str, logger: Logger) -> str | None:
    try:
        for line in contents.splitlines():
            if '"event":"Commander"' not in line:
                continue
            return json.loads(line)["Name"]
    except Exception as ex:
        logger.exception(f"Failed to parse File. Exception: {ex}")
    return None


def __get_rank_from_journal(contents: str, logger: Logger) -> int | None:
    try:
        for line in contents.splitlines():
            if '"event":"Rank"' not in line:
                continue
            return json.loads(line)["Combat"]
    except Exception as ex:
        logger.exception(f"Failed to parse File. Exception: {ex}")
    return None
