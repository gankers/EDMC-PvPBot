# 1.2.0: The Update Update
## Features
- Add Autoupdate and Prerelease Checks

## Bugfixes
- **CMDRs that are not in a Squadron no longer error on submitting Live kills** 
- Make Historic Aggregation more robust by ignoring Encoding Issues
- Skip over bad parses of journal entries
- Add additional logging to figure out why some users don't get live kill submissions
- Delay writing API Key Errors until UI is ready
## Tooling / CI
- We no longer use the `versions` File. The correct version is extracted from the Tag and injected into the version check file by the CI/CD Pipeline
- `ruff` config was corrected so it now actually checks for issues
# 1.1.0: The Powerplay Update
## Features
- Major Refactoring of the Plugin to make individual components testable
- Write Integration Tests that take a journal file and run it through the historic aggregate
- Powerplay Allegiances are now tracked
- Add Current Version to Settings Page for easier debugging
- Add Reset Toggle to Current Power to settings
- Add K/D Reporting when Targeting a Player
## Bugfixes
- Most Recent Squadron is no longer applied to old Events when rerunning historic aggregate
- Debug Info can now be hidden
- System Hiding Flag now considered for submissions