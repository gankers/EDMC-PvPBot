import threading
import time
import tkinter
from typing import Any
from edmc_abstractions import EDMCProxy
from event_handler import EventHandler, push_event_to_handler
from shared_types import PvpKill, DebugEventHandlerState


def prefs_changed_impl(self: EDMCProxy, cmdr: str):
    self.config().ui(cmdr).commit()
    self.do_api_key_check()


def plugin_prefs_impl(self: EDMCProxy, cmdr: str, parent_frame: tkinter.Frame):
    return self.config().ui(cmdr).init(parent_frame)


def plugin_start3_impl(self: EDMCProxy) -> str:
    self.logger().info("Starting PVP Bot Plugin")
    if self.config().check_updates and not self.config().onboarding:
        self.logger().info("Starting Update Check in new Thread...")

        def update_check_thread():
            while self.ui() is None:
                time.sleep(0.5)
                self.logger().debug("Deferring Interacting UI until its ready")
            result = self.update_handler().check()
            if result is None:
                self.ui().write_error_message(
                    "Couldn't check for updates. \n"
                    "Please contact dev if this persists.",
                    -1,
                )
            if not self.update_handler().is_current_version_outdated():
                return  # Up to date

            if self.config().autoupdate:
                self.update_handler().run_autoupdate()
            else:
                self.ui().notify_about_outdated()

        threading.Thread(
            name="pvp-update-check", daemon=True, target=update_check_thread
        ).start()
    else:
        self.logger().info(
            "Skipping Update Check because it is disabled in the settings."
        )

    self.do_api_key_check()
    return self.plugin_name()


def plugin_app_impl(self: EDMCProxy, parent: tkinter.Frame) -> tkinter.Frame:
    """
    The calling context (real or mock) usually does the magic beforehand
    """
    self.put_parent_frame(parent)
    return parent


def journal_entry_impl(
    self: EDMCProxy,
    cmdr: str,
    system: str,
    entry: dict[str, Any],
    state: dict[str, Any],
):
    # Some cursed Schrödinger's Fix
    # For some reason SquadronStartup events aren't consistently passed to the handler
    # without this log. See https://github.com/EDCD/EDMarketConnector/issues/2286
    if entry["event"] == "SquadronStartup":
        self.logger().info(
            f"Received SquadronStartup with Squadron {entry['SquadronName']}"
        )

    if not self.config().is_cmdr_valid(cmdr):
        return

    def debug_handler(handler: DebugEventHandlerState):
        if not self.config().debug:
            handler = None
        ui = self.ui()
        if ui is not None:
            ui.write_debug_info(handler)

    if cmdr not in self.live_handlers:
        self.logger().info(f"Creating new EventHandler for CMDR {cmdr}")
        self.live_handlers[cmdr] = EventHandler(
            self.config(),
            self.logger(),
            cmdr,
            state["Rank"]["Combat"][0],
            system=system,
            debug_cb=debug_handler,
        )
        if "ShipType" in state:
            self.live_handlers[cmdr].put_ship_update(state["ShipType"])

        # Allow debugging (if the relevant Config is set), Historic does not have this
        self.live_handlers[cmdr].enable_ui_push()
        self.live_handlers[cmdr].set_targeted_callback(self.do_whois_lookup)

        def submit_callback(event: PvpKill):
            """
            Called by a live handler if a Live Submit is taking place.
            This callback here will
            1. resolve Squadrons
            2. submit Kill
            3. Report back result to UI (if possible)
            """
            try:
                squadrons_needing_lookup = [
                    f.name
                    for f in [event.victim_squadron, event.killer_squadron]
                    if f is not None and f.tag is None
                ]
                squadron_cache = self.squadrons_api().get_data_for_list_of_squadrons(
                    squadrons_needing_lookup
                )
                err = self.pvp_api().submit_live_kill(event, squadron_cache)
                if err is None:
                    self.ui().write_happy_message("PVPBot: Submitted successfully", 5)
                else:
                    self.ui().write_error_message(
                        "PVPBot: Error submitting.\nCheck logs for more infos.", 10
                    )
            except Exception as err:
                self.logger().error("Something broke trying to submit live event")
                self.logger().exception(err)

        self.live_handlers[cmdr].set_update_callback(submit_callback)

    push_event_to_handler(self.live_handlers[cmdr], entry, self.logger())
