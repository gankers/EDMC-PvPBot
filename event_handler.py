import threading
from datetime import datetime, timedelta
from logging import Logger
from typing import Optional, Callable

from edmc_abstractions import IEventHandler
from plugin_config import Configuration
from shared_types import PvpKill, SquadronData, DebugEventHandlerState


class EventHandler(IEventHandler):
    """
    "Live" and "Historic" Events are handled the same way.
    Relevant Journal Entries are passed to this class.
    It keeps track of an internal state like the current CMDR Name,
    current Targeting Cache, current System, etc.

    This is a STATEFUL class.
    A LIVE Event Handler is run for each CMDR in parallel to allow for multiboxing,
    although support from EDMC is… lacking. So don't expect this to actually work
    """

    def enable_ui_push(self):
        pass

    def __init__(
        self,
        config: Configuration,
        logger: Optional[Logger],
        cmdr: str,
        rank: int,
        system: Optional[str] = None,
        ship: Optional[str] = None,
        debug_cb: Optional[Callable[[Optional[DebugEventHandlerState]], None]] = None,
    ):
        self.__targeting_cb: Optional[Callable[[str], None]] = None
        self.__system = system
        self.__logger = logger
        self.__own_cmdr: str = cmdr
        self.__config = config
        self.__own_ship = ship
        self.__last_targeted: Optional[str] = None
        self.__last_targeted_time = datetime.now()
        """
        Only used to debounce whois requests
        """
        self.__target_cache: dict[str, tuple[datetime, str]] = {}
        self.__target_squadron_cache: dict[str, str | None] = {}
        """
        This makes the assumption that a squadron does not change during an instance. 
        I guess that assumption is fair to make.
        """
        self.__target_power_cache: dict[str, str | None] = {}

        self.__own_rank = rank
        self.__pvpkill_cb: Optional[Callable[[PvpKill], None]] = None
        self.debug_cb = debug_cb

    # callback is assumed to be daemon-safe
    def set_update_callback(self, cb: Optional[Callable[[PvpKill], None]]):
        self.__pvpkill_cb = cb

    # assume the callback to create its own thread!
    def set_targeted_callback(self, cb: Optional[Callable[[str], None]]):
        self.__targeting_cb = cb

    def put_system_update(self, system: str):
        self.__system = system
        self.__build_and_emit_debug()

    def put_targeted_event(
        self,
        targeted_cmdr: str,
        targeted_ship: str,
        ts: datetime,
        targeted_squadron: Optional[str],
        targeted_power: Optional[str],
    ):
        self.__target_cache[targeted_cmdr] = (ts, targeted_ship)
        self.__target_squadron_cache[targeted_cmdr] = targeted_squadron
        self.__target_power_cache[targeted_cmdr] = targeted_power
        if self.__targeting_cb is not None and (
            self.__last_targeted != targeted_cmdr
            or (datetime.now() - self.__last_targeted_time) > timedelta(seconds=5)
        ):
            self.__targeting_cb(targeted_cmdr)
            self.__last_targeted_time = datetime.now()
        self.__last_targeted = targeted_cmdr

        self.__build_and_emit_debug()

    def put_ship_update(self, ship_name: str):
        self.__own_ship = ship_name
        self.__build_and_emit_debug()

    def clear_cache(self):
        self.__target_cache.clear()
        self.__build_and_emit_debug()

    def put_death(
        self, killer: str, killer_rank: int, killer_ship: Optional[str], ts: datetime
    ):
        kill_event = PvpKill(
            system=self.__system,
            victim=self.__own_cmdr,
            victim_ship=self.__own_ship,
            victim_rank=self.__own_rank,
            killer=killer,
            killer_ship=killer_ship,
            killer_rank=killer_rank,
            kill_timestamp=ts,
            killer_squadron=None,
            victim_squadron=self.__config.get_squadron_for_cmdr(self.__own_cmdr),
            killer_power=self.__target_power_cache.get(killer),
            victim_power=self.__config.get_power_for_cmdr(self.__own_cmdr),
        )

        own_squad = self.__config.get_squadron_for_cmdr(self.__own_cmdr)
        if own_squad:
            kill_event.victim_squadron = SquadronData(name=own_squad, tag=None)
        if killer in self.__target_squadron_cache:
            kill_event.killer_squadron = SquadronData(
                name=None, tag=self.__target_squadron_cache[killer]
            )
        self.__push_kill(kill_event)
        self.clear_cache()
        self.__build_and_emit_debug()

    def put_kill(self, victim: str, victim_rank: int, ts: datetime):
        victim_ship = None

        if victim in self.__target_cache:
            ts_scan, ship = self.__target_cache[victim]

            if ts_scan + timedelta(minutes=5) > ts:
                # if here, the Scan is still valid.
                # if you manage to target someone (without retarget) for 5 Minutes
                # straight and STILL not manage to kill them… then uhh… bruh
                victim_ship = ship
            # Delete so a scenario like Killing ship followed by On-Foot does not
            # eval as 2 ship-kills in quick succession
            del self.__target_cache[victim]

        kill_event = PvpKill(
            system=self.__system,
            killer=self.__own_cmdr,
            killer_ship=self.__own_ship,
            killer_rank=self.__own_rank,
            victim=victim,
            victim_ship=victim_ship,
            victim_rank=victim_rank,
            kill_timestamp=ts,
            killer_squadron=None,
            victim_squadron=None,
            killer_power=self.__config.get_power_for_cmdr(self.__own_cmdr),
            victim_power=self.__target_power_cache.get(victim),
        )
        own_squad = self.__config.get_squadron_for_cmdr(self.__own_cmdr)
        if own_squad:
            kill_event.killer_squadron = SquadronData(name=own_squad, tag=None)
        if victim in self.__target_squadron_cache:
            kill_event.victim_squadron = SquadronData(
                name=None, tag=self.__target_squadron_cache[victim]
            )

        self.__push_kill(kill_event)
        self.__build_and_emit_debug()

    def __push_kill(self, kill_event: PvpKill):
        if self.__logger is not None:
            self.__logger.info(
                "[PVP Bot Handler] Compiled the following PVP Kill Event: %s",
                kill_event,
            )

        cb = self.__pvpkill_cb
        if cb is None:
            if self.__logger is not None:
                self.__logger.warning(
                    "[PVP Bot Handler]: Must ignore Kill event because callback "
                    "was not defined!"
                )
        else:
            # We are currently on the main thread here. This is no bueno, because the
            # callback is long-running as it will call the API and so on.
            # We spawn a new thread here
            def thread_fn():
                cb(kill_event)

            threading.Thread(
                daemon=True, name="pvpbot-kill-submit-daemon", target=thread_fn
            ).start()

    def __build_and_emit_debug(self):
        cb = self.debug_cb
        if cb is not None:
            debug_data = DebugEventHandlerState(
                system=self.__system,
                own_cmdr=self.__own_cmdr,
                own_ship=self.__own_ship,
                target_cache=self.__target_cache,
                own_rank=self.__own_rank,
                own_squadron=self.__config.get_squadron_for_cmdr(self.__own_cmdr),
            )
            cb(debug_data)

    def put_own_squadron(self, squadron_long_name: str | None):
        self.__config.set_squadron_for_cmdr(self.__own_cmdr, squadron_long_name)

    def put_own_power(self, power_name: Optional[str]):
        self.__config.set_power_for_cmdr(self.__own_cmdr, power_name)

    def put_own_rank(self, rank: int):
        self.__own_rank = rank


relevant_events = [
    "LoadGame",  # Used for current ship, current CMDR Name
    "Loadout",  # Used when ship is updated
    "Rank",  # Used for own Rank
    "Location",  # StarSystem Data used for current System
    "Embark",  # Used to figure out when we go into an SRV or the Ship
    "SuitLoadout",  # Emitted when disembarking - we use this to figure out our own suit
    "LaunchSRV",  # Emitted when SRV is deployed.
    # We don't care about the DockSRV Event as a Loadout Event always follows
    "SupercruiseEntry",  # Reconcile System
    "FSDJump",  # Update Location
    "ShipTargeted",  # Used to potentially infer a Kill Victim's Ship
    "PVPKill",  # Used when we kill another player. Contains Name, Time and Combat Rank
    "Died",  # Used when killed by another player,
    "SquadronStartup",  # Used to infer the player's Squadron
    "JoinedSquadron",  # Used to infer the player's Squadron
    "DisbandedSquadron",  # Used to infer the player's Squadron
    "KickedFromSquadron",  # Used to infer the player's Squadron
    "LeftSquadron",  # Used to infer the player's Squadron
    "Powerplay",  # Used to infer the player's Power
]
"""
Contains a list of all names that should be consumed by a listener. 
If an event is **NOT** in this list, it gets ignored. For example 
Text Messages or Wing Events are completely irrelevant to our use case.
"""


def push_event_to_handler(handler: IEventHandler, entry: dict, logger: Logger | None):
    event_name = entry["event"]
    assert isinstance(event_name, str)

    if event_name not in relevant_events:
        return

    timestamp = datetime.fromisoformat(entry["timestamp"])
    # I wish for Rust-esque Match Statements in Python :C
    match event_name:
        case "LoadGame" | "Loadout":
            if "Ship" in entry:
                handler.put_ship_update(entry["Ship"])
        case "Rank":
            handler.put_own_rank(entry["Combat"])
        case "CarrierJump":
            if "Docked" in entry and entry["Docked"]:
                handler.put_system_update(entry["StarSystem"])
        case "Location":
            handler.put_system_update(entry["StarSystem"])
        case "SupercruiseEntry":
            handler.clear_cache()
            handler.put_system_update(entry["StarSystem"])
        case "FSDJump":
            handler.put_system_update(entry["StarSystem"])
            handler.clear_cache()
        case "Powerplay":
            handler.put_own_power(entry["Power"])
        case "Embark":
            if entry["SRV"]:
                handler.put_ship_update("SRV")
            else:
                # No need to do anything here.
                # After Embark onto the Ship, a Loadout Event is triggered,
                # which will tell us the ship
                pass
        case "SuitLoadout":
            # We pretend our Suit is a ship.
            # Is this scuffed? Yes!
            # Does FDEV pull the same shit? Also yes!
            handler.put_ship_update(entry["SuitName"])
        case "LaunchSRV":
            if "SRVType" not in entry:
                # This can be undefined for ancient events
                # (back when the Scorpion was not in the game)
                entry["SRVType"] = (
                    "testbuggy"
                    # SRV Scarab is called "testbuggy" internally… why? fuck knows.
                )

            handler.put_ship_update(entry["SRVType"])
        case "ShipTargeted":
            if "ScanStage" not in entry or entry["ScanStage"] < 1:
                return  # Irrelevant
            pilot_name_raw = entry["PilotName"]
            assert isinstance(pilot_name_raw, str)
            prefix = "$cmdr_decorate:#name="

            if not pilot_name_raw.startswith(prefix):
                # Target not a CMDR. We don't care
                return
            cmdr_target_name = pilot_name_raw[len(prefix) : -1]

            handler.put_targeted_event(
                cmdr_target_name,
                entry["Ship"],
                timestamp,
                entry.get("SquadronID"),
                entry.get("Power"),
            )
        case "SquadronStartup" | "JoinedSquadron":
            handler.put_own_squadron(entry["SquadronName"])
            if logger is not None:
                logger.info(
                    "Put Info about Squadron into Event Handler:"
                    + entry["SquadronName"]
                )
        case "LeftSquadron" | "DisbandedSquadron" | "KickedFromSquadron":
            handler.put_own_squadron(None)
        case "PVPKill":
            handler.put_kill(entry["Victim"], entry["CombatRank"], timestamp)
        case "Died":
            cmdr: str
            ship: str
            str_rank: str
            # Yes, this is a very consistent API by a very competent Company
            if "Killers" in entry:
                cmdr = entry["Killers"][0]["Name"]
                ship = entry["Killers"][0]["Ship"]
                str_rank = entry["Killers"][0]["Rank"]
            elif (
                "KillerName" not in entry
                or "KillerShip" not in entry
                or "KillerRank" not in entry
            ):
                # Died Event is irrelevant because it's a self-inflicted Death, lol
                return
            else:
                cmdr = entry["KillerName"]
                ship = entry["KillerShip"]
                str_rank = entry["KillerRank"]

            if not cmdr.lower().startswith("cmdr "):
                # We don't have an actual CMDR here. Probably died to an NPC
                # We don't care about those events :)
                return

            cmdr = cmdr[len("cmdr ") :]
            handler.put_death(cmdr, _rank_str_to_int_lookup(str_rank), ship, timestamp)

        case _:
            raise NotImplementedError()


def _rank_str_to_int_lookup(str_rank: str) -> int:
    __rank_lookup = {
        "harmless": 0,
        "mostly harmless": 1,
        "novice": 2,
        "competent": 3,
        "expert": 4,
        "master": 5,
        "dangerous": 6,
        "deadly": 7,
        "elite": 8,
    }

    if str_rank.lower() not in __rank_lookup:
        raise ValueError(f"{str_rank} does not seem to be a valid Rank.")

    return __rank_lookup[str_rank.lower()]
