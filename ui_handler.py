"""
All things UI are handled here.
Receives Events from Main or Worker Threads which are then handled on the Main Thread

UI MUSTN'T be modified from daemonic / non-main threads!
"""

import threading
import time
from logging import Logger
import tkinter
from typing import Optional, Callable

import version_check
from edmc_abstractions import IAPIClient, ISquadronsClient, IUiHandler
from onboarding import Onboarding
from plugin_config import Configuration
from shared_types import ThemeStub, UiState, DebugEventHandlerState
from version_check import UpdateInfo

try:
    from theme import theme
except:  # noqa E722
    theme = ThemeStub()


class PvpBotUi:
    """
    Handles the main UI.
    Settings UI is in the plugin_config.py Module
    """

    def __init__(
        self,
        parent: tkinter.Frame,
        logger: Logger,
        update_info: UpdateInfo,
        happy_message_cb: Optional[Callable[[str, int], None]] = None,
        error_message_cb: Optional[Callable[[str, int], None]] = None,
    ):
        self.__parent = parent
        self.__ok_cb = happy_message_cb
        self.__err_cb = error_message_cb
        self.__logger = logger
        self.__state = UiState.default()
        self.__update = update_info
        self.__parent.bind("<<Update_PVPKill_UI>>", lambda _: self.update_from_event())

    # any thread
    def update_ui_state(self, mutator: Callable[[UiState], None]):
        mutator(self.__state)
        self.__parent.event_generate("<<Update_PVPKill_UI>>")

    # only_ui_thread
    def __build_debug_state_container(self, parent: tkinter.Frame) -> tkinter.Frame:
        data = self.__state.debug_data
        assert data is not None
        debug_frame = tkinter.Frame(parent)

        lines = [
            f"CMDR {f} flying {data.target_cache[f][1]} ({data.target_cache[f][0]})"
            for f in data.target_cache
        ]
        lines.insert(
            0,
            f"DEBUG: \nCMDR {data.own_cmdr} in {data.system} flying {data.own_ship}"
            f"with Rank {data.own_rank}",
        )
        if data.own_squadron:
            lines.insert(1, "Own Squad: " + data.own_squadron)

        for idx, entry in enumerate(lines):
            tkinter.Label(debug_frame, text=entry).grid(row=idx)

        theme.update(debug_frame)
        return debug_frame

    def set_debug_state(self, payload):
        def mutator(state: UiState, _timers=None):
            state.debug_data = payload

        self.update_ui_state(mutator)

    # only_ui_thread
    def update_from_event(self):
        """
        This Method is called by tk's Event Loop. It is always run on the Main Thread!
        This should be invoked after the State was modified.

        WARNING: Actions in here MUST NOT call update_ui_state
        directly to avoid a Deadlock.
        """
        # First Clear Everything in the Parent (should only be 1 Child at most)
        for child in self.__parent.winfo_children():
            child.destroy()
        self.__parent.grid()
        # Now Construct a new Frame
        frame = tkinter.Frame(self.__parent)
        if self.__state.display_update:

            def open_download():
                self.__update.open_download_page()

            # Build a New Frame with 2 Buttons, one opening the Gitlab Release Page,
            # another one Dismissing the Message
            update_frame = tkinter.Frame(frame)
            update_frame.config(pady=10)
            info_text = "".join(
                [
                    "PvpBot Plugin is Outdated\nv",
                    version_check.CURRENT_VERSION(),
                    " → v",
                    self.__update.remote_version,
                ]
            )
            tkinter.Label(update_frame, text=info_text).grid(columnspan=3)
            btn_download = tkinter.Button(
                update_frame, text="Go to Download", command=open_download
            )

            def trigger_update():
                # Create a new thread that does the update
                def update_thread():
                    try:
                        self.__update.run_autoupdate()
                        if self.__ok_cb is not None:
                            self.__ok_cb(
                                "Autoupdate complete. "
                                "Restart EDMC to switch to new version",
                                -1,
                            )
                    except Exception as err:
                        self.__logger.error("Failed to run autoupdater")
                        self.__logger.exception(err)
                        if self.__err_cb is not None:
                            self.__err_cb(
                                "Failed to run autoupdater.\n"
                                "Tell the devs if this persists.\n"
                                "More Info in the logs",
                                -1,
                            )

                threading.Thread(
                    name="pvp-updater", daemon=True, target=update_thread
                ).start()

            btn_update = tkinter.Button(
                update_frame, text="Autoupdate", command=trigger_update
            )

            def mutator_dismiss(state: UiState, _timers=None):
                state.display_update = False

            btn_dismiss = tkinter.Button(
                update_frame,
                text="Dismiss",
                command=lambda: self.update_ui_state(mutator_dismiss),
            )

            for i, item in enumerate([btn_download, btn_update, btn_dismiss]):
                item.grid(row=1, column=i)
            theme.update(update_frame)
            update_frame.grid(sticky=tkinter.EW)

        if self.__state.display_success:
            tkinter.Label(frame, text=self.__state.display_success, fg="green").grid()
        if self.__state.display_error:
            tkinter.Label(frame, text=self.__state.display_error, fg="red").grid()
        if self.__state.debug_data:
            self.__build_debug_state_container(frame).grid()

        frame.grid(sticky="ew")
        theme.update(frame)
        self.__parent.grid()


class UiHandler(IUiHandler):
    def __init__(
        self,
        parent: tkinter.Frame,
        config: Configuration,
        logger: Logger,
        squadron_client: ISquadronsClient,
        pvp_client: IAPIClient,
        update_info: UpdateInfo,
    ) -> None:
        self.__is_onboarding = config.onboarding
        self.__parent_frame = parent
        self.__logger = logger
        self.__update_info = update_info
        self.__ui_instance: Optional[PvpBotUi] = None
        self.__ui_reset_counter_happy = 0
        self.__ui_reset_counter_error = 0
        """
        Whenever a new message is pushed, this is added by one. 
        When the timer for a message expires, it checks if this number is 
        the same as its assigned number.
        Only if the timers number is <= the current number do we actually
        unset the message.
        """
        if self.__is_onboarding:
            self.__onboarding_instance = Onboarding(
                parent,
                config,
                logger,
                squadron_client,
                pvp_client,
                self.__finish_onboarding,
            )
            self.__onboarding_instance.render()
        else:
            self.__ui_instance = PvpBotUi(
                parent,
                logger,
                update_info,
                self.write_happy_message,
                self.write_error_message,
            )

    def __finish_onboarding(self):
        """
        This must be called by the Onboarding Instance,
        and the Onboarding Instance only!
        """
        if self.__is_onboarding:
            self.__is_onboarding = False
            self.__onboarding_instance = None
            self.__ui_instance = PvpBotUi(
                self.__parent_frame,
                self.__logger,
                self.__update_info,
                self.write_happy_message,
                self.write_error_message,
            )
        else:
            raise ValueError(
                "Illogical State. Cannot finish onboarding because "
                "it is currently not active"
            )

    def write_debug_info(self, handler: DebugEventHandlerState | None):
        ui = self.__ui_instance
        if ui is None:
            # Not much we can do here
            return
        ui.set_debug_state(handler)

    def notify_about_outdated(self):
        if self.__ui_instance is not None:

            def update_state(a: UiState):
                a.display_update = True

            self.__ui_instance.update_ui_state(update_state)

    def write_error_message(self, message, duration_secs):
        if self.__ui_instance is None:
            self.__logger.warning(
                "Tried to write Error Message too early: UI Instance not initialized"
            )
            return

        def reset_task(own_number: int, delay: float):
            time.sleep(delay)
            if self.__ui_reset_counter_error <= own_number:

                def mutator(a: UiState):
                    a.display_error = None

                self.__ui_instance.update_ui_state(mutator)

        def writer(a: UiState):
            a.display_error = message

        self.__ui_instance.update_ui_state(writer)
        if duration_secs > 0:
            self.__ui_reset_counter_error += 1
            threading.Thread(
                name="pvpbot-ui-delayer",
                daemon=True,
                target=reset_task,
                args=(self.__ui_reset_counter_error + 0, duration_secs),
            ).start()

    def write_happy_message(self, message, duration_secs):
        if self.__ui_instance is None:
            self.__logger.warning(
                "Tried to write Happy Message too early: UI Instance not initialized"
            )
            return

        def reset_task(own_number: int, delay: float):
            time.sleep(delay)
            if self.__ui_reset_counter_happy <= own_number:

                def mutator(a: UiState):
                    a.display_success = None

                self.__ui_instance.update_ui_state(mutator)

        def writer(a: UiState):
            a.display_success = message

        self.__ui_instance.update_ui_state(writer)
        if duration_secs > 0:
            self.__ui_reset_counter_happy += 1
            threading.Thread(
                name="pvpbot-ui-delayer",
                daemon=True,
                target=reset_task,
                args=(self.__ui_reset_counter_happy + 0, duration_secs),
            ).start()
