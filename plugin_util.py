from typing import Any


def _rank_str_to_int_lookup(str_rank: str) -> int:
    __rank_lookup = {
        "harmless": 0,
        "mostly harmless": 1,
        "novice": 2,
        "competent": 3,
        "expert": 4,
        "master": 5,
        "dangerous": 6,
        "deadly": 7,
        "elite": 8,
    }
    result = __rank_lookup.get(str_rank.lower())
    if result is None:
        raise ValueError(f"{str_rank} does not seem to be a valid Rank.")
    return result


def ui_build_progress_bar(fraction: float, text_len: int):
    fraction_chars = ["▏", "▎", "▍", "▌", "▋", "▊", "▉", "█"]
    full_chars = int(fraction * float(text_len))

    progress_bar = full_chars * "█"

    if fraction < 1:
        # Determine for "fraction" for this one character
        local_fraction = (fraction % 1.0 / float(text_len)) * float(text_len)
        char_idx = int(local_fraction * len(fraction_chars))

        progress_bar += fraction_chars[char_idx]

    progress_bar += (text_len - full_chars - 1) * "▔"
    return progress_bar


def assertNotNone(entries: list[Any]):
    for idx, entry in enumerate(entries):
        if entry is None:
            raise ValueError(f"{idx} argument is none")
