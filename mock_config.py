from typing import Any

from edmc_abstractions import IConfig


class TestMockConfig(IConfig):
    state: dict[str, Any] = {}

    def set(self, key: str, val: Any):
        self.state[key] = val

    def get_str(self, key: str, default=None) -> str:
        result = self.state.get(key)
        if result is None:
            return default
        if not isinstance(result, str):
            raise ValueError("not a string")
        return result

    def get_bool(self, key: str, default=None) -> bool:
        result = self.state.get(key)
        if result is None:
            return default
        if not isinstance(result, bool):
            raise ValueError("not a bool")
        return result

    def get_app_name(self) -> str:
        return "mock"
