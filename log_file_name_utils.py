import re
from datetime import datetime, timezone


def _is_horizons_file(filename: str):
    # e.g. Journal.220212014914.01.log
    pattern = re.compile("^Journal\\.\\d*\\.\\d*\\.log$")
    return pattern.fullmatch(filename) is not None


def get_timestamp_from_filename(filename: str):
    utc_datetime = datetime.now(timezone.utc)
    try:
        if _is_horizons_file(filename):
            timestamp_text = "20" + filename.split(".")[1]
            return utc_datetime.strptime(timestamp_text, "%Y%m%d%H%M%S")
        else:
            # Oddy: Journal.2022-05-20T170535.01.log
            timestamp_text = filename.split(".")[1]
            return utc_datetime.strptime(timestamp_text, "%Y-%m-%dT%H%M%S")
    except:  # noqa E722
        return None
